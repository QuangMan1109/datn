const express = require("express");

let fb = "fb"

//init app
const app = express();
const port = 3000;

const multer = require("multer");

const path = require("path");

const fs = require("fs");

//set storage engine
const storage = multer.diskStorage( {
    destination:    "./public/uploads/",

    filename:       function(req, file, cb) {
        console.log(file);
        cb(null, Date.now() + "-" + file.originalname );
    }
});

//init upload
const upload = multer( {
    storage: storage
} ).single("avatar")


//ejs
app.set("view engine", "ejs");
app.set("views", "./views");


//public folder
app.use(express.static("./public"));


//init socket io
const server = require("http").Server(app);
const io = require("socket.io")(server);
server.listen(process.env.PORT || 3000);


/*-------------------- ĐỌC SVG FILE --------------------*/


let defaultSvgPath = `./public/resources/C-letter.svg`;
let svgPath_SERVER = defaultSvgPath;

let readSvgFunc = require("./readSvg.js").readSvgFunc;
let roundPsArr = require(`./readSvg.js`).roundPsArr;
let readSvgConstants = require(`./readSvg.js`).readSvgConstants;

let drawPs = [];
let nDrawPs = [];
let drawObjSent = {};

let updateDrawArrays = ( {svgPath} ) => {
    let {views, drawArrays} = readSvgFunc({
        svgPath:    svgPath_SERVER,
    })

    drawPs = drawArrays.drawPs;
    nDrawPs = drawArrays.nDrawPs;
}

let initDrawArrays = updateDrawArrays( {
    svgPath:    svgPath_SERVER,
})

initDrawArrays;

const PACK_SIZE = 40;
const MAX = readSvgConstants.MAX;
let Point = readSvgConstants.Point;
let DCNN = 100;


/*-------------------- GET POST --------------------*/
app.get("/", function(req, res) {
    if (code == 0) {
        res.render("pre-trangchu.ejs");
    }

    else {
        res.render("trangchu.ejs")
    }
});

let code = 0;

// app.get("/main", (req, res) => {
//     if (code == 1) {
//         res.render("trangchu.ejs")
//     }
// })



/*-------------------- GET /test --------------------*/
let nthSent = 0;

let clientDoneDrawing = true;
let drawCommandYet = false;

let xHomePoint = 0;
let yHomePoint = 0;

let zoomIndex = 1;

let drawPsSent = [];
let nDrawPsSent = [];

app.get("/test", (req, res) => {

    let drawPsSent = [];
    let nDrawPsSent = [];

    // console.log(req.query.nth);
    if (req.query.nth == undefined) {
        nthSent = 0;

        if (req.query.client_done == 1) {
            clientDoneDrawing = true;
            console.log(`client done drawing`);
            res.send(`Server acknowledged`);
        }
        
    }
    else {
        nthSent = parseInt(req.query.nth);

        // cat array drawPs duoc gui thanh cac goi co do dai PACK_SIZE, bat dau tu phan tu iStart, ket thuc iEnd
        let iStart = nthSent * PACK_SIZE;
        let iEnd = (nthSent + 1) * PACK_SIZE;
        if (iEnd > drawPs.length) {
            iEnd = drawPs.length;

            for (let i = iStart; i < iEnd; i ++) {
                drawPsSent.push(new Point(drawPs[i].x, drawPs[i].y) )
            }
            // drawPsSent = drawPs.slice(iStart, iEnd);

            //  Thêm toạ độ home point vào
            for (let i = 0; i < drawPsSent.length; i++) {
                drawPsSent[i].x = drawPsSent[i].x - xHomePoint;
                drawPsSent[i].y = drawPsSent[i].y - yHomePoint;
            }
        }

        else {
            for (let i = iStart; i < iEnd; i ++) {
                drawPsSent.push(new Point(drawPs[i].x, drawPs[i].y) )
            }

            // console.log(xHomePoint, yHomePoint);

            //  Thêm toạ độ home point vào
            // console.log(drawPsSent);
            for (let i = 0; i < drawPsSent.length; i++) {
                drawPsSent[i].x = drawPsSent[i].x - xHomePoint;
                drawPsSent[i].y = drawPsSent[i].y - yHomePoint;
            }
            // console.log(drawPsSent);
        }

        // console.log(`\niStart = ${iStart}, iEnd = ${iEnd}`);


        // cat array nDrawPs duoc gui, bat dau tu iNStart, iNEnd
        // nDrawPs[k] = 181, tuc la tai diem 180 cua mang drawPs thi se khong noi vao diem 182
        let iNStart = 0;
        let iNEnd = 0;

        for (let i = 0; i < nDrawPs.length; i++) {
            let k1 = nDrawPs[i] + 1;
            if (k1 >= iStart) {
                for (let j = i; j <= nDrawPs.length; j++) {
                    let k2;
                    if (j == nDrawPs.length) {
                        k2 = MAX;
                    }
                    else {
                        k2 = nDrawPs[j] + 1;
                    }
                    if (k2 > iEnd) {
                        iNStart = i;
                        iNEnd = j;
                        break;
                    }
                }
                break;           
            }
        }
        nDrawPsSent = nDrawPs.slice(iNStart, iNEnd);
        

        // dua 2 mang drawPsSent va nDrawPsSent vao 1 object de gui
        // done la key the hien da gui xong
        if (iEnd == drawPs.length) {
            drawObjSent.done = 1;
        }
        else {
            drawObjSent.done = undefined;
        }

        roundPsArr(drawPsSent, 1);

        drawObjSent.drawPs = drawPsSent;
        drawObjSent.nDrawPs = nDrawPsSent;

        res.send(drawObjSent);
        nthSent++;

    }
})

app.get(`/drawCommandYet`, (req, res) => {
    if (drawCommandYet) {
        res.send(`Yes`);
        drawCommandYet = false;
        clientDoneDrawing = false;
    }
    else {
        res.send(`No`);
    }
})

app.post("/upload", function(req, res) {

    upload(req, res, function(err) {
        if (err) {
            res.render("trangchu.ejs", {
                msg: err
            } )
        }
        else{
            if (typeof req.file == "undefined") {
                res.render("trangchu.ejs", {
                    msg: "Error: No file sellected!"
                } );
            }
            else{
                console.log(1);
                res.render("trangchu.ejs", {
                    msg: "Upload file successfully!",
                    file: `uploads/${req.file.filename}`,
                    fileExtension:  path.extname(req.file.filename),
                });
            }
        }
    });
});


// const downloadFile = require(`./Downloader`).downloadFile;
// let url = `https://static.depositphotos.com/storage/bgremove/processed_59f80a12-7d3c-4c5f-afe4-9ee49b6843c1_60d73e51990f1.png?isize=600`;
// downloadFile(url, path.resolve(`./public/quang-rm.png`), (filename) => {
//     console.log(filename);
// })


/*-------------------- CHUYỂN ĐỔI ẢNH SANG FILE SVG --------------------*/

let removeBackground = require("./web_scraping/remove_background.js").removeBackground
let vectorize = require("./web_scraping/vectorize.js").vectorize;
let outline = require("./web_scraping/outline.js").outline;

let updateSvgAtServer = () => {
    let {views, drawArrays} = readSvgFunc( {
        svgPath:            svgPath_SERVER,
        customZoomIndex:    zoomIndex,
    });

    console.log(views);

    //  Cập nhật giá trị mảng drawPs tại server
    drawPs = drawArrays.drawPs;
    nDrawPs = drawArrays.nDrawPs;

    return {views, drawArrays};
}

const {Builder, By, Key, until, Capabilities, WebElement, Actions} = require("selenium-webdriver");
require("chromedriver");
const chrome = require("selenium-webdriver/chrome");

let options = new chrome.Options();
options.setChromeBinaryPath(process.env.CHROME_BINARY_PATH)
let serviceBuilder = new chrome.ServiceBuilder(process.env.CHROME_DRIVER_PATH);

io.on("connection", function(socket) {

    console.log(socket.id + " da ket noi den server!");
    code = 0;

    socket.on("disconnect", function() {
        console.log(socket.id + " da ngat ket noi!");
        console.log("-----------------------------");

        removeAllFilesOfAFolder('./public/uploads');
        removeAllFilesOfAFolder('./public/removebgs');
        removeAllFilesOfAFolder('./public/outlines');
        removeAllFilesOfAFolder('./public/vectorizes');
    });

    socket.on("Go to Main Page", () => {
        code = 1;
    })

    socket.on(`client-read-default-svg-file`, (exampleFile) => {
        svgPath_SERVER = `./public/resources/${exampleFile}`;
        console.log(`${socket.id} muốn đọc file svg mặc định: ${svgPath_SERVER}`);

        let {views, drawArrays} = updateSvgAtServer();

        socket.emit(`server-send-updated-svg-info`, {views, drawArrays});

        console.log(`server đã trả thông tin đọc được cho client: ${socket.id}` );
    })

    socket.on(`client-send-zoomIndex`, (index) => {
        zoomIndex = index;
        console.log(`Cập nhật giá trị zoom index: ${zoomIndex}`);

        let {views, drawArrays} = updateSvgAtServer();

        socket.emit(`server-send-updated-svg-info`, {views, drawArrays})
    })

    socket.on("client-draw-up", (data) => {
        drawPs = [];
        nDrawPs = [];
        drawPs[0] = new Point(0, 0);
        drawPs[1] = new Point(0, -1 * DCNN);
        nDrawPs[0] = -1;
        
        console.log("Di chuyển lên");
    })

    socket.on("client-draw-right", (data) => {

        drawPs = [];
        nDrawPs = [];
        drawPs[0] = new Point(0, 0);
        drawPs[1] = new Point(DCNN, 0);
        nDrawPs[0] = -1;

        console.log("Di chuyển sang phải");
    })

    socket.on("client-moveto", (data1, data2) => {

        drawPs = [];
        nDrawPs = [];
        drawPs[0] = new Point(0, 0);
        drawPs[1] = new Point(parseFloat(data1), parseFloat(data2) );
        nDrawPs[0] = -1;

        console.log("Di chuyển đến điểm xy");
    })

    socket.on(`client-removeBg`, async (uploadPath) => {

        let [result, err] = await removeBackground({
            Builder:            Builder,
            By:                 By,
            until:              until,
            options:            options,
            serviceBuilder:     serviceBuilder,

            browserHeadless:    true,
            srcPath:            `./public/${uploadPath}`
        }, (filename) => {
            let removedBgFile = path.basename(filename);
            socket.emit(`server-send-removedBg-path`, removedBgFile);
        } )

        if (err) {
            console.log(err);
            socket.emit(`server-send-unsuccessful-removeBg`, err);
        }

    })

    socket.on("client-outline", async (date, uploadPath, removeBgStatus, thresholdInp, blurInp, thresholdMax, blurMax) => {
        console.log(`client muốn lấy ảnh outline: ${uploadPath}, removeBgStatus = ${removeBgStatus}, threshold = ${thresholdInp}/${thresholdMax}, blur = ${blurInp}/${blurMax}`);

        let srcPath = `./public/${uploadPath}`;
        let [outlinedImg, err] = await outline({
            Builder:            Builder,
            By:                 By,
            until:              until,
            options:            options,
            serviceBuilder:     serviceBuilder,

            date:               date,
            browserHeadless:    true,
            srcPath:            srcPath,
            removeBgStatus:     removeBgStatus,
            thresholdInp:       thresholdInp,
            blurInp:            blurInp,
            thresholdMax:       thresholdMax,
            blurMax:            blurMax,
        })

        if (!err) {
            let srcImg = `${path.basename(outlinedImg)}`
            socket.emit(`server-send-outlined-file`, srcImg);
            console.log(`server đã trả outlined file: ${srcImg}`);
        }
        else {
            socket.emit(`server-send-unsuccessful-outline`)
            console.log(err);
        }
    })

    socket.on("client-vectorize", async (outlinedFile) => {
        console.log(`Client muốn vectorize từ ảnh: ${outlinedFile}`);
        await vectorize({
            outlinedFile:   outlinedFile,
        }, (filename, err) => {

            if (!err) {

                socket.emit(`server-send-vertorized-path`, filename);
                console.log(`server đã trả vectorized path`);

            }

            else {
                socket.emit(`server-send-unsuccessful-outline`)
                console.log(err);
            }

        })
    })

    socket.on(`client-load-file`, (srcSvgFile, uploadSvgFile) => {

        if (uploadSvgFile) {
            console.log(`client wants to update svg file`);
            console.log(srcSvgFile);
            svgPath_SERVER = `./public/${srcSvgFile}`;

            let {views, drawArrays} = updateSvgAtServer();
    
            socket.emit(`server-send-updated-svg-info`, {views, drawArrays});
        }


        else {
            console.log(`client wants to update svg file`);
            console.log(srcSvgFile);
            svgPath_SERVER = `./public/vectorizes/${srcSvgFile}`;
    
            let {views, drawArrays} = updateSvgAtServer();
    
            socket.emit(`server-send-updated-svg-info`, {views, drawArrays});
        }

    })

    socket.on(`client-set-home-point`, (x, y) => {
        console.log(`client vừa set home point, server cập nhật lại nhé: ${x}, ${y}`);

        xHomePoint = x;
        yHomePoint = y;
    })

    socket.on(`let Mr.Machine draw`, () => {
        console.log(`browser client gửi lệnh vẽ xuống client máy vẽ`);
        drawCommandYet = true;
        socket.emit(`server let Mr.Machine draw`);

        let machineFinishHisWorkTimeout = setTimeout( () => {
            if (!clientDoneDrawing) {
                console.log(`Sau 15ph, machine vẫn chưa vẽ xong, vui lòng kiểm tra lại nhe`);
                clientDoneDrawing = true;
                socket.emit(`server-force-MrMachine-to-stop`);
            }
        }, 15 * 60 * 1000)
    })
});

let removeAllFilesOfAFolder = (directory) => {
    fs.readdir(directory, (err, files) => {
        if (err) throw err;

        for (const file of files) {
            if ( !path.basename(file).includes(`nothing.txt`) ) {
                fs.unlink( path.join(directory, file), err => {
                    if (err) throw err;
                } )
            }

        }

    })

    console.log(`Đã xoá tất cả files ở folder ${directory}`);
}