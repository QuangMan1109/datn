// var socket = io("http://localhost:3000");
var socket = io("https://esp8266-sendreq.herokuapp.com");


var baseCanvas = document.getElementById('baseCanvas');
var homePointCanvas = document.getElementById(`homePointCanvas`);


const selectors = {
    fileExtension:          $("#fileExtension"),

    selection:              $(".svgSamples .selection"),

    convertArea:            $('.convertArea'),

    removeBgConverterBtn:   $("#removeBgConvertBtn"),

    thresholdInp:           $("#thresholdInp"),
    blurInp:                $("#blurInp"),
    outlineConvertBtn:      $("#outlineConvertBtn"),
    outlinedImg:            $(".outlineArea .display .image"),

    balanceInp:             $("#balanceInp"),
    resolutionInp:          $("#resolutionInp"),
    vectorizeConvertBtn:    $("#vectorizeConvertBtn"),

    loadFileBtn:            $(".loadFileArea input"),

    letMrMachineDraw:       $("#letMrMachineDraw"),
    debuggerImg1:           $(".debugArea .image1"),
    debuggerImg2:           $(".debugArea .image2"),
    debuggerImg3:           $(".debugArea .image3"),

    xHomePoint:             $(".homePoint input.x"),
    yHomePoint:             $(".homePoint input.y"),
    zoomIndex:              $(".zoomArea .zoomIndex"),

    time:                   $(`.timeArea span.time`)
}

class CONVERSION_STATUS {
    constructor(isNotYet, isDoing, isDone) {
        this.isNotYet = isNotYet;
        this.isDoing = isDoing;
        this.isDone = isDone;
    }
}

const NOT_YET_STATUS = new CONVERSION_STATUS(true, false, false);
const DOING_STATUS = new CONVERSION_STATUS(false, true, false);
const DONE_STATUS = new CONVERSION_STATUS(false, false, true);

let wVP;
let hVP;

let drawPs = [];
let nDrawPs = [];

let donePreviewing = false;


let baseContext = baseCanvas.getContext("2d");
baseContext.lineWidth = 1;

homePointCanvas.width = 1000;
homePointCanvas.height = 1000;
let homePointContext = homePointCanvas.getContext(`2d`);
homePointContext.lineWidth = 2;

let homePointRadius = 3;

let uploadStatus = `not_yet`;
let removeBgStatus = `not_yet`;
let outlineStatus = `not_yet`;
let vectorizeStatus = `not_yet`;

let removedBgFile = ``;
let outlinedFile = ``;
let vectorizedFile = ``;

let srcOutlinedImg = ``;
let srcSvgFile = ``;
let uploadSvgFile = true;


/*-------------------- UPDATE CANVAS --------------------*/
let updateCanvas = ({views}) => {//    Update mỗi lần server thay đổi
    setView(views);

    if (!donePreviewing) {        
        draw();
        donePreviewing = true;
    }

    setHomePoint(homePointCanvas.width/2, homePointCanvas.height/2, homePointRadius, `blue`);
}

let setView = (views) => {//    set view cho default svg file và update svg file

    console.log(views);
    
    baseCanvas.width = views.width;
    baseCanvas.height = views.height;

    homePointCanvas.width = views.width;
    homePointCanvas.height = views.height;
}

let setDefaultValue = ({//  Đặt giá trị mặc định cho các input: home point và chuyển đổi
    thresholdDefaultValue,
    blurDefaultValue,
}) => {

    //  input cho việc chuyển đổi 
    selectors.thresholdInp.val(thresholdDefaultValue);
    selectors.blurInp.val(blurDefaultValue);
}


/*-------------------- UPLOAD HANDLER --------------------*/
let uploadHandler = () => {
    if ( selectors.fileExtension.html() == ".svg" ) {
        doneUploadingSvgFile();
    }

    else if (       selectors.fileExtension.html() == ".jpg"
                ||  selectors.fileExtension.html() == ".JPG" 
                ||  selectors.fileExtension.html() == ".jpeg"
                ||  selectors.fileExtension.html() == ".JPEG" 
                ||  selectors.fileExtension.html() == ".gif"
                ||  selectors.fileExtension.html() == ".GIF"                 
                ||  selectors.fileExtension.html() == ".png"
                ||  selectors.fileExtension.html() == ".PNG" ){
        doneUploadingImgFile();
    }
}

let doneUploadingSvgFile = () => {//    hàm thực thi khi file uploaded là file svg
    console.log(`hi`);
    selectors.convertArea.attr(`style`, `display: none;`);
    selectors.loadFileBtn.removeAttr('disabled');
    srcSvgFile = $("#uploadPath").html();
    console.log(srcSvgFile);
    uploadSvgFile = true;

}

let doneUploadingImgFile = () => {//    hàm thực thi khi file uploaded là file ảnh
    uploadSvgFile = false;

    selectors.convertArea.removeAttr(`style`)
}

/*-------------------- WAIT UNTIL MR.MACHINE FINISH HIS WORK --------------------*/
let waitUntilMrMachineFinishHisWork = () => {
    console.log(`Server đã gửi lệnh vẽ xuống Mr.Machine`);

    //  Disable nút nhấn gửi lệnh vẽ
    selectors.letMrMachineDraw.attr(`value`, `Please wait until Mr.Machine finish his work!`);
    selectors.letMrMachineDraw.attr(`disabled`, true);

    //  Disable selection svg samples
    selectors.selection.attr(`disabled`, true);
}

/*-------------------- SERVER FORCE MR.MACHINE TO STOP --------------------*/
let serverForceMrMachineToStop = () => {
    console.log(`Sau 1 khoảng thời gian, máy vẽ vẫn chưa vẽ xong, vui lòng kiểm tra lại`);

    //  Disable nút nhấn gửi lệnh vẽ
    selectors.letMrMachineDraw.attr(`value`, `Let Mr.Machine draw this!!!`);
    selectors.letMrMachineDraw.removeAttr(`disabled`);

    //  Disable selection svg samples
    selectors.selection.removeAttr(`disabled`);
}

$(document).ready(function() {

    uploadHandler();

    setTimeout(closeSpan, 2000);

    setHomePoint(homePointCanvas.width/2, homePointCanvas.height/2, homePointRadius, `blue`);

    socket.emit(`client-read-default-svg-file`, `C-letter.svg`);
    socket.emit("client-send-zoomIndex", selectors.zoomIndex.val() );

    selectors.selection.on(`change`, () => {
        donePreviewing = false;
        socket.emit(`client-read-default-svg-file`, selectors.selection.val());
    })

    setDefaultValue({// đặt giá trị mặc định cho các range input
        thresholdDefaultValue:      50,
        blurDefaultValue:           20,
    });

    /*-------------------- REMOVE BACKGROUND AREA --------------------*/
    selectors.removeBgConverterBtn.click( () => {//   nếu client nhấn nút remove background
        
        removeBgStatus = `doing`;

        let uploadPath = $("#uploadPath").html();
        socket.emit(`client-removeBg`, uploadPath);

        selectors.removeBgConverterBtn.attr('disabled', 'true');
    } )

    /*-------------------- OUTLINE AREA --------------------*/
    $("#thresholdInp").change( () => {//    nếu thresholdInput thay đổi

        if (outlineStatus != `doing`) {
            $("#outlineConvertBtn").removeAttr('disabled')
        }
    })

    $("#blurInp").change( () => {// nếu blurInput thay đổi

        if (outlineStatus != `doing`) {
            $("#outlineConvertBtn").removeAttr('disabled')
        }
    })

    $("#outlineConvertBtn").click( () => {//   nếu client nhấn nút outline
        outlining = true;
        outlineStatus = `doing`;

        //  Đọc 2 giá trị input
        let thresholdInp = $("#thresholdInp").val();
        let blurInp = $("#blurInp").val();
        let thresholdMax = $("#thresholdInp").attr('max');
        let blurMax = $("#blurInp").attr('max');

        //  emit lên server
        let uploadPath = $(`#uploadPath`).html();
        socket.emit(`client-outline`, Date.now(), uploadPath, removeBgStatus, thresholdInp, blurInp, thresholdMax, blurMax);

        // xoá notification
        $(".outlineDisplay .notification").html(``);

        // //  image loading
        // $(".outlineArea .display .image").attr("src", 'loader')
        // $(".outlineArea .display .image").attr('alt', 'Outlining the image. Please wait')
        // $(".outlineArea .display .image").show();
        
        //  Disable removeBg area
        selectors.removeBgConverterBtn.attr('disabled', 'true');

        //  Disable outline area
        selectors.thresholdInp.attr('disabled', 'true');
        selectors.blurInp.attr('disabled', 'true');
        selectors.outlineConvertBtn.attr('disabled', 'true');

        //  Disable vectorize area
        selectors.resolutionInp.attr('disabled', 'true');
        selectors.balanceInp.attr('disabled', 'true');
        selectors.vectorizeConvertBtn.attr('disabled', 'true');
    })

    /*-------------------- VECTORIZE AREA --------------------*/
    $("#vectorizeConvertBtn").click( () => {
        vectorizeStatus = `doing`;

        //  emit lên server
        socket.emit(`client-vectorize`, outlinedFile);

        //  disable nút outline
        $("#vectorizeConvertBtn").attr('disabled', 'true');
        $("#thresholdInp").attr('disabled', 'true');
        $("#blurInp").attr('disabled', 'true');

        //  disable nút load file
        selectors.loadFileBtn.attr('disabled', 'true')

        // xoá notification
        $(".vectorizeDisplay .notification").html("")

        //  image loading
        $("#vectorizeImg").attr("src", 'loader')
        $("#vectorizeImg").attr('alt', 'Vectorizing the image. Please wait')
        $("#vectorizeImg").show();

        //  disable 2 input
        $("#balanceInp").attr('disabled', 'true');
        $("#resolutionInp").attr('disabled', 'true');        
    })

    /*-------------------- LOAD FILE --------------------*/
    $(".loadFileArea input").click( () => {
        socket.emit(`client-load-file`, srcSvgFile, uploadSvgFile)
    })

    $("#btnFile").click( () => {
        $("#realbtnFile").click();
    } )

    $("#realbtnFile").change( () => {
        let val = $("#realbtnFile").val().match( /[\/\\]([\w\d\s\.\-\(\)]+)$/)[1];
        $("#txtCustom").html(val)

        $("#btnSubmit").removeAttr("hidden");
    })

    selectors.zoomIndex.change( () => {
        if (!isNaN( parseFloat( selectors.zoomIndex.val() ) )) {
            socket.emit("client-send-zoomIndex", selectors.zoomIndex.val() );
        }
    })

    $("#btnUp").click( () => {
        socket.emit("client-draw-up");
    })
    
    $("#btnRight").click( () => {
        socket.emit("client-draw-right");
    })

    $("#btnMoveTo").click( () => {
        socket.emit("client-moveto", $("#ipToX").val() , $("#ipToY").val() );
    })

    $("#btnGrid").click( () => {
        let val = $("#btnGrid").val();

        if ( val == "Grid On" ) {
            gridOn();
            $("#btnGrid").val("Grid Off");
        }

        else {
            gridOff();
            $("#btnGrid").val("Grid On");
        }
    } )

    selectors.letMrMachineDraw.click( () => {
        console.log(`Client just clicked letMrMachineDraw button`);
        socket.emit(`let Mr.Machine draw`);
    })

    selectors.xHomePoint.on(`change`, () => {
        let x = selectors.xHomePoint.val();
        let y = selectors.yHomePoint.val();
        setHomePoint(x, y, homePointRadius, `blue`, true);
    })

    selectors.yHomePoint.on(`change`, () => {
        let x = selectors.xHomePoint.val();
        let y = selectors.yHomePoint.val();
        setHomePoint(x, y, homePointRadius, `blue`, true);
    })
});

socket.on(`server-send-default-svg-info`, ( {views, drawArrays} ) => {

    console.log(views);

    drawPs = drawArrays.drawPs;
    nDrawPs = drawArrays.nDrawPs;
    console.log(`drawPs.len = ${drawPs.length}`);
    console.log(drawPs);

    updateCanvas({
        views:  views,
    })
})

socket.on("test-fb", (fb) => {
    $("#txtMsg2").append(fb)
})

socket.on(`server-send-removedBg-path`, (removedBgFile) => {

    console.log(`Client đã nhận removed file: ${removedBgFile}`);
    removeBgStatus = `done`;
    removedBgFile = removedBgFile;

    //  Hiện ảnh đã removeBg
    $(".removeBgDisplay .image").removeAttr('hidden');
    $(".removeBgDisplay .image").attr('src', `removebgs/${removedBgFile}`)

    //  Hiện image debug
    selectors.debuggerImg1.attr(`src`, `facebook-sign-in-page.png`);
    selectors.debuggerImg2.attr(`src`, `approveAccess-3.png`);
    selectors.debuggerImg3.attr(`src`, `approveAccess-4.png`);
})

socket.on(`server-send-unsuccessful-removeBg`, (error) => {
    console.log(error);
    removeBgStatus = `not_yet`;

    //  Enable nút remove background
    selectors.removeBgConverterBtn.removeAttr('disabled');

    selectors.debuggerImg1.attr(`src`, `approveAccess.png`);
    selectors.debuggerImg2.attr(`src`, `approveAccess-2.png`);
    selectors.debuggerImg3.attr(`src`, `approveAccess-3.png`);
})

socket.on(`server-send-outlined-file`, (srcImg) => {
    outlineStatus = `done`;

    outlinedFile = srcImg;
    console.log(`Client nhận outlined file: ${outlinedFile}`);

    //  Hiện ảnh
    selectors.outlinedImg.attr("src", `outlines/${outlinedFile}`)

    //  Enable 2 input
    $("#thresholdInp").removeAttr('disabled');
    $("#blurInp").removeAttr('disabled');

    //  Hiện vectorizeArea
    selectors.resolutionInp.removeAttr('disabled');
    selectors.balanceInp.removeAttr(`disabled`);
    selectors.vectorizeConvertBtn.removeAttr(`disabled`)
})

socket.on(`server-send-unsuccessful-outline`, () => {
    console.log(`Có lỗi khi outline`);
    outlineStatus = `not_yet`;
    outlinedFile = ``;

    //  Enable 2 input
    $("#thresholdInp").removeAttr('disabled');
    $("#blurInp").removeAttr('disabled');

    $(".outlineArea .display .image").hide();
    $(".outlineDisplay .notification").html("Outline không thành công, vui lòng thử lại")
})

socket.on(`server-send-vertorized-path`, (src) => {
    srcSvgFile = src;
    console.log(`đã nhận vectorized file ${src}`);
    vectorizing = false;

    //  Hiện ảnh
    $(".vectorizeArea .display .image").attr("src", `vectorizes/${src}`)

    //  Enable threshold và blur ở outline area
    $("#thresholdInp").removeAttr('disabled');
    $("#blurInp").removeAttr('disabled');

    //  Enable 2 input
    $("#balanceInp").removeAttr('disabled');
    $("#resolutionInp").removeAttr('disabled');

    //  Enable nút Load file
    $(".loadFileArea input").removeAttr('disabled');
})

socket.on(`server-send-updated-svg-info`, ( {views, drawArrays} ) => {
    //  Cập nhật giá trị chưa vẽ xong
    donePreviewing = false;
    
    drawPs = drawArrays.drawPs;
    nDrawPs = drawArrays.nDrawPs;
    console.log(`drawPs.len = ${drawPs.length}`);
    
    updateCanvas({
        views:  views,
    })
})

socket.on(`server let Mr.Machine draw`, () => {
    waitUntilMrMachineFinishHisWork();
})

socket.on(`server-force-MrMachine-to-stop`, () => {

})

function closeSpan() {
    $("#txtMsg1").html("");
    $("#iconMsg1").hide();
    console.log("1");
}

function setHomePoint(x, y, r, color, inputChanged) {// Hàm set home point
    if (!inputChanged) {    //  Nếu là thay đổi input thì không đặt lại input về mặc định
        selectors.xHomePoint.val(homePointCanvas.width/2);
        selectors.yHomePoint.val(homePointCanvas.height/2);
    } 

    //  Xoá home point trước đó
    homePointContext.clearRect(0, 0, homePointCanvas.width, homePointCanvas.height)
    
    //  Vẽ home point mới
    homePointContext.beginPath();
    homePointContext.moveTo(0, 0);
    homePointContext.arc(x, y, r, 0, Math.PI * 2, true);
    homePointContext.fillStyle = color;
    homePointContext.fill();

    //  Hiện kích thước
    homePointContext.font = "bold 15px Courier New";
    homePointContext.fillStyle = `#5D534A`;
    homePointContext.fillText(`${homePointCanvas.width}x${homePointCanvas.height}`, 10, 20)

    socket.emit(`client-set-home-point`, selectors.xHomePoint.val(), selectors.yHomePoint.val());
}

function draw() {
    baseContext.beginPath();
    baseContext.lineWidth = 1;
    // emuArr();

    baseContext.moveTo(0, 0);

    let k = 0;
    let x = 0;
    let y = 0;
    let xPreI = 0;
    let yPreI = 0;
    let preDraw = 0;

    let preX = 0;
    let preY = 0;

    let totalLen = 0;
    let liftTimes = 0;
    for (let i = 0; i < drawPs.length; i++) {

        x = drawPs[i].x;
        y = drawPs[i].y;

        if (i > 0) {
            xPreI = drawPs[i - 1].x;
            yPreI = drawPs[i - 1].y;
        }
        if (i == nDrawPs[k] + 1) {
            
            penDown = 0;
            if (preDraw == 1) {
                liftTimes++;
            }
            // move(baseContext, x, y, penDown)
            
            if (k < nDrawPs.length - 1) {
                k++;
            }
            preDraw = 0;
        }
        else {

            penDown = 1;
            if (preDraw == 0) {
                // let len = Math.sqrt( (xPreI - preX) * (xPreI - preX) + (yPreI - preY) * (yPreI - preY) )
                let len = Math.abs(xPreI - preX) + Math.abs(yPreI - preY)
                totalLen += len;
                move(baseContext, xPreI, yPreI, 0)
                preX = xPreI;
                preX = yPreI;

                liftTimes++;
            }
            
            // let len = Math.sqrt( (x - preX) * (x - preX) + (y - preY) * (y - preY) )
            let len = Math.abs(x - preX) + Math.abs(y - preY);
            totalLen += len;
            move(baseContext, x, y, penDown)
            preDraw = 1;
            preX = x;
            preY = y;

        }
    }

    baseContext.stroke();
    console.log(`totalLen = ${totalLen}, liftTimes = ${liftTimes}`)
    let time = Math.round( totalLen * 0.01 + liftTimes * 0.105)
    selectors.time.html(`${time}s`)


    // let base64 = baseCanvas.toDataURL().split(`,`)[1];
    // console.log(base64);
}

function pause(milliseconds) {
	var dt = new Date();
	while ( (new Date()) - dt <= milliseconds ) { /* Do nothing */ }
}

function move(baseContext, x, y, penDown) {
    if (!penDown) {
        
        baseContext.moveTo(x, y);
    }

    else {
        baseContext.lineTo(x, y);
    }
}

function gridOn() {
    baseContext.beginPath();
    baseContext.lineWidth = 0.2;

    for (let i = 0; i <= 10; i++) {
        baseContext.moveTo(wVP/10 * i, 0);
        baseContext.lineTo(wVP/10 * i, hVP);
    }
    for (let i = 0; i <= 10; i++) {
        baseContext.moveTo(0, hVP/10 * i);
        baseContext.lineTo(wVP, hVP/10 * i);
    }

    // baseContext.clearRect(0, 0, 1000, 1000);
    
    baseContext.stroke();
    
}

function gridOff() {
    baseContext.clearRect(0, 0, wVP, hVP);
    draw();
}

module.exports.CONVERSION_STATUS = CONVERSION_STATUS;
module.exports.NOT_YET_STATUS = NOT_YET_STATUS;
module.exports.DOING_STATUS = DOING_STATUS;
module.exports.DONE_STATUS = DONE_STATUS;