const puppeteer = require('puppeteer');
    const path = require('path');
    const downloadPath = path.resolve('./download');
    console.log(downloadPath);
    async function simplefileDownload() {
        const browser = await puppeteer.launch({
            headless: false
        });
        
        const page = await browser.newPage();
        await page.goto(
            'https://unsplash.com/photos/tn57JI3CewI',
            { waitUntil: 'networkidle2' }
        );

        const client = await page.target().createCDPSession();
        
        await client.send('Page.setDownloadBehavior', {
            behavior: 'allow',
            downloadPath: downloadPath
        });
        await page.click('._2vsJm ')
    }
    simplefileDownload();