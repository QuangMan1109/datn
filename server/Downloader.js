const https = require(`https`);
const fs = require(`fs`);
const path = require(`path`);

let downloadFile = (url, filename, callback) => {

    const req = https.get(url, (res) => {
        const fileStream = fs.createWriteStream(filename);
        res.pipe(fileStream);

        fileStream.on(`error`, (err) => {
            console.log(`Error writing to the stream`);
            console.log(err);
        })

        fileStream.on(`close`, () => {
            callback(filename);
        })

        fileStream.on(`finish`, () => {
            fileStream.close();
            console.log(`Done!`);
        })

        req.on(`error`, (err) => {
            console.log(`Error downloading the file`);
            console.log(err);
        })
    })
}

module.exports.downloadFile = downloadFile;
