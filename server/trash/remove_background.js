const puppeteer = require("puppeteer");
const fs = require("fs");
const path = require("path");
const downloadFile = require(`../Downloader`).downloadFile;

let browser;

const WEB_SCRAPING_CONSTANTS = require("./constants.js").WEB_SCRAPING_CONSTANTS;

const PAGE_LOADING_TIMEOUT = WEB_SCRAPING_CONSTANTS.PAGE_LOADING_TIMEOUT;
const SELECTOR_TIMEOUT = WEB_SCRAPING_CONSTANTS.SELECTOR_TIMEOUT;
const REMOVEBG_DOWNLOAD_PATH = WEB_SCRAPING_CONSTANTS.REMOVEBG_DOWNLOAD_PATH;

const REMOVE_SRC_PATH = WEB_SCRAPING_CONSTANTS.REMOVE_SRC_PATH;
const REMOVEBG_DEFAULT_DES_PATH = WEB_SCRAPING_CONSTANTS.REMOVEBG_DEFAULT_DES_PATH;
const REMOVEBG_CUSTOM_DES_PATH = WEB_SCRAPING_CONSTANTS.REMOVEBG_CUSTOM_DES_PATH;

const funcs = require("./functions.js").funcs;
const { resolve } = require("path");
let typeInput = funcs.typeInput;
let verifyDownload = funcs.verifyDownload;
let renameFile = funcs.renameFile;


const FACEBOOK_URL = "https://facebook.com";
const FACEBOOK_USERNAME = "duytran17.neu@gmail.com";
const FACEBOOK_PASSWORD = "duy_1234";

const GMAIL_URL = "https://accounts.google.com"
const GMAIL_USERNAME = 'duytran17.neu@gmail.com';
const GMAIL_PASSWORD = 'xinchaotoiladuy3'

const removeBgUrl = "https://depositphotos.com/bgremover/upload.html";

const selectors = {
    chooseFileBtn:          '._2aOTt',
    downloadBtn:            '._3sbXw button',
    signInBtn:              '.signup-user-2020__login-link',
    gmailSignInBtn:         '.login-user__social-box div:nth-child(1)',
    facebookSignInBtn:      '.login-user__social-box div:nth-child(2)',
    usernameGmailInp:       'input[type=email]',
    // usernameGmailNextBtn:   '#identifierNext',
    usernameGmailNextBtn:   '#identifierNext',
    passwordGmailInp:       'input[type=password]',
    passwordGmailNextBtn:   '#passwordNext',
}

let loginFacebook = async () => { // ĐĂNG NHẬP FACEBOOK
    console.log(`BƯỚC 1.1: ĐĂNG NHẬP VÀO FACEBOOK.COM ...`)
        
    const fbTab = await browser.newPage();

    await fbTab.browser().version().then(function(version) {
        console.log(version);
        });

    // await fbTab.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36 Edg/91.0.864.54');

    try {           //  Truy cập facebook
        await fbTab.goto(
            FACEBOOK_URL,
            {timeout: PAGE_LOADING_TIMEOUT, waitUntil: "networkidle2"}
        );
    } catch (error) {
        await fbTab.close();
        return Promise.reject(new Error(`Không truy cập được ${FACEBOOK_URL}`) );
    }
    
    // Nhập username
    let fbSltor = "#email";
    await fbTab.$eval(fbSltor, (elm, FACEBOOK_USERNAME) => {
        elm.value = FACEBOOK_USERNAME;
    }, FACEBOOK_USERNAME)

    // Nhập password
    fbSltor = "#pass";
    await fbTab.$eval(fbSltor, (elm, FACEBOOK_PASSWORD) => {
        elm.value = FACEBOOK_PASSWORD;
    }, FACEBOOK_PASSWORD)

    // Nhấn đăng nhập
    fbSltor = "._6ltg";
    await fbTab.click(fbSltor);
    console.log("Đang đăng nhập");

    
    await fbTab.waitFor(3000)
    await fbTab.screenshot({
        path:       `./public/facebook-sign-in-page.png`,
        fullPage:   true,
    })

    // let div = await fbTab.evaluate(() => document.body.innerHTML);
    // console.log(div);


    try {   // Chờ load trang facebook.com
        await fbTab.waitForSelector(".p361ku9c", {timeout: SELECTOR_TIMEOUT * 1.5});
        console.log(`Đăng nhập ${FACEBOOK_URL} thành công`);     
    } catch (error) {
        await browser.close();
        return Promise.reject(new Error(`Đăng nhập ${FACEBOOK_URL} không thành công`))
    }

    await fbTab.close();
    return Promise.resolve("Đăng nhập facebook thành công")
}

let goToRemoveBackgroundPage = async ( {browserHeadless, srcPath} ) => {    // REMOVE BACKGROUND
    console.log(`-> Chuyển đến trang https://depositphotos.com/bgremover/upload.html và thực hiện removeBg ...`);

    const rmBgTab = await browser.newPage();
    // await rmBgTab.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');
    try {   //  Truy cập vào trang để remove
        await rmBgTab.goto(
            removeBgUrl,
            {waitUntil: "networkidle2", timeout: PAGE_LOADING_TIMEOUT}
        );     
    } catch (error) {
        await rmBgTab.close();
        return Promise.reject(new Error(`Không thể truy cập ${removeBgUrl}`) )
    }

    try {
        //  chọn thư mục download
        await rmBgTab._client.send("Page.setDownloadBehavior", {
            behavior: "allow",
            downloadPath: REMOVEBG_DOWNLOAD_PATH,
        });

        // let div0 = await rmBgTab.evaluate(() => document.body.innerHTML);
        // console.log(div0);

        //  Chọn file upload
        await rmBgTab.waitForSelector(selectors.chooseFileBtn);    
        const [fileChooser] = await Promise.all( [
            rmBgTab.waitForFileChooser(),
            rmBgTab.click(selectors.chooseFileBtn)
        ] )

        await fileChooser.accept( [srcPath] );
        console.log(`-> Chọn file thành công, đợi upload`);

        //  Nhấn download
        await rmBgTab.waitForSelector(selectors.downloadBtn);
        // await rmBgTab.click(selectors.downloadBtn);
        console.log(`-> Upload thành công, chuẩn bị download`);

        let url = await rmBgTab.$eval(`._3WVj0`, img => img.src);
        console.log(url);

        return [url, null];

    } catch (error) {
        return [null, error];
    }


    // //  Nhấn đăng nhập
    // await rmBgTab.waitForSelector(selectors.signInBtn);
    // await rmBgTab.click(selectors.signInBtn)

    // console.log(`-> Chọn hình thức đăng nhập`);

    // //  Nhấn đăng nhập với facebook
    // await rmBgTab.waitForSelector(selectors.facebookSignInBtn);
    // await rmBgTab.click(selectors.facebookSignInBtn)
    // console.log(`-> Đang đăng nhập với facebook`);

    // await rmBgTab.waitFor(3000);
    // await rmBgTab.screenshot({
    //     path:       `./public/fb.png`,
    //     fullPage:   true,
    // })

    // //  Nhấn đăng nhập với gmail
    // await rmBgTab.waitForSelector(selectors.gmailSignInBtn);
    // await rmBgTab.click(selectors.gmailSignInBtn)
    // console.log(`-> Đang đăng nhập với gmail`);

    // const newPagePromise = new Promise(x => browser.once('targetcreated', target => x( target.page() ) ) );
    // const gmailTab = await newPagePromise;
    // console.log(`-> Tab đăng nhập gmail xuất hiện`);

    // return Promise.resolve({
    //     rmBgTab:    rmBgTab,
    //     gmailTab:   gmailTab,
    // });
}

let signInGmail = async( {browserHeadless, gmailTab} ) => {
    

    if (browserHeadless) {
        /*-------------------------- HEADLESS------------------------*/
        await gmailTab.waitForSelector(selectors.usernameGmailInp);
        await typeInput({
            page:       gmailTab,
            selector:   selectors.usernameGmailInp,
            value:      GMAIL_USERNAME,
        })
    
        await gmailTab.click('input[type=submit]')
        console.log('-> Vừa click nút next của username');
    
        await gmailTab.waitForSelector('input[type=password]');
        await typeInput({
            page:       gmailTab,
            selector:   'input[type=password]',
            value:      GMAIL_PASSWORD,
        })
        
        await gmailTab.click('input[type=submit]')
        console.log('-> Vừa click nút next của password');

        // await gmailTab.waitFor(2000)
        // await gmailTab.keyboard.press('PageDown')
    
    
        await gmailTab.waitFor(3000);

        await gmailTab.screenshot({
            path:       `./public/approveAccess.png`,
            fullPage:   true,
        })

        console.log(`-> Vừa đợi 3s và chụp ảnh: ./public/approveAccess.png`);




        // let inputIdS = await gmailTab.$$eval('input', elms => elms.map(elm => elm.getAttribute('id')));
        // console.log(inputIdS);
        // let inputClassS = await gmailTab.$$eval('input', elms => elms.map(elm => elm.getAttribute('class')));
        // console.log(inputClassS);
        // let buttonIdS = await gmailTab.$$eval('button', elms => elms.map(elm => elm.getAttribute('id')));
        // console.log(buttonIdS);
        // let buttonClassS = await gmailTab.$$eval('button', elms => elms.map(elm => elm.getAttribute('class')));
        // console.log(buttonClassS);
        // let buttonTypeS = await gmailTab.$$eval('button', elms => elms.map(elm => elm.getAttribute('type')));
        // console.log(buttonTypeS);

        // let div = await gmailTab.evaluate(() => document.body.innerHTML);
        // console.log(div);

        // try {
        //     await typeInput({
        //         page:       gmailTab,
        //         selector:   "input[type=email]",
        //         value:      `manvanquang1109@gmail.com`,
        //     })

        //     await gmailTab.screenshot({
        //         path:       `./public/approveAccess-2.png`,
        //         fullPage:   true,
        //     })

        //     await gmailTab.click(`#submit`)
        // }
        // catch {
        //     try{
        //         await gmailTab.click(`button[type=submit]`);

        //         await gmailTab.waitFor(2000);

        //         await gmailTab.screenshot({
        //             path:       `./public/approveAccess-3.png`,
        //             fullPage:   true,
        //         })


        //         div = await gmailTab.evaluate(() => document.body.innerHTML);
        //         console.log(div);
        //     }
            
        //     catch{
        //         await gmailTab.screenshot({
        //             path:       `./public/approveAccess-4.png`,
        //             fullPage:   true,
        //         })

        //         await gmailTab.click('#submit_approve_access');
        //         console.log(`click ok chua`);
        //     }
        // }


        try {
            await gmailTab.click('#submit_approve_access');
            console.log(`-> Click nút allow`);
        } catch (error) {
            
        }
    }

    else {
        /*-------------------------- NORMAL------------------------*/
        await gmailTab.waitForSelector(selectors.usernameGmailInp);
        
        console.log('tim thay username input');
        await typeInput({
            page:       gmailTab,
            selector:   selectors.usernameGmailInp,
            value:      GMAIL_USERNAME,
        })
        console.log('type thanh cong');

        await gmailTab.click(selectors.usernameGmailNextBtn)
        console.log(5);


        await gmailTab.waitForSelector(selectors.passwordGmailInp);
        console.log(5.5);


        await gmailTab.waitFor(2000);
        await typeInput({
            page:       gmailTab,
            selector:   selectors.passwordGmailInp,
            value:      GMAIL_PASSWORD,
        })


        // await newPage.waitForSelector(selectors.passwordGmailNextBtn);
        await gmailTab.click(selectors.passwordGmailNextBtn)
    }

}

let removeBackground = async ( {browserHeadless, srcPath}, callback ) => {   //remove background function
    
    try {
        console.log(`BƯỚC 1: REMOVE BACKGROUND CỦA ẢNH ...`);
        browser = await puppeteer.launch( {
            headless: browserHeadless,
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox'
            ],
        } );

        // await loginFacebook();
        let [url, error] = await goToRemoveBackgroundPage({
                browserHeadless:    true,
                srcPath:            srcPath,
        });

        if (!error) {
            downloadFile(url, REMOVEBG_CUSTOM_DES_PATH(srcPath), (filename) => {
                callback(filename);
            })
        }

        else {
            return [null, error];
        }
    
        await browser.close();

    } catch (error) {
        console.error(`Xảy ra lỗi khi Remove background, vui lòng thử lại`)
        return [null, error];
    }

}

module.exports.removeBackground = removeBackground;