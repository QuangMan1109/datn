const domParser = require("dom-parser");
const parser = new domParser();

const fs = require("fs");

const filePath = "public/uploads/khung.svg";
const MAX = 100000;
const CURVE_RESOLUTION = 10;

class Point{
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    set(x, y) {
        this.x = x;
        this.y = y;
    }
}

let dAtts = [];

let drawCmd = [];
let m2ndIndex = [];
let cmd = [];
let cmdCnt = [];

let cmdPs = [];
let absPs = [];

let drawPs = [];
let nDrawPs = [];


function myfun(filePath, cb) {
    fs.readFile("public/uploads/home-origin.svg", "utf-8",
    function(err, html){

        if (err) throw err;

        cb(html);

    });
}

myfun(filePath, function(html) {
    const dom = parser.parseFromString(html);
    var pathElements = dom.getElementsByTagName("path");
    
    pathElements.forEach(function(item, index) {
        var dAtt = item.getAttribute("d");
        dAtts.push(dAtt);
        
    });
    
    dAtts.forEach(function(item, index) {
        console.log(`--------------ITEM ${index}--------------`);
        console.log(item);

        clrArr();

        //chuan hoa dAtt
        stdItem = stdStr(item);

        //cNumsArr: chars and numbers Array
        let cNumsArr = stdItem.split(" ");

        extract(cNumsArr);

        toAbsCoord();

        build_drawPsArr();

        prtArr();
    });
});

function stdStr(s0) {

    //s1 la chuoi chi giu lai cac ky tu can thiet, gom '-' '.' 0 den 9, xoa khoang trang hai dau
    let s1 = s0.replace(/[^-.0-9MHVLQTCSAZmhvlqtcsaz]/g, " ").trim();

    //s2 la chuoi se tra ve, khoi tao s2 = s1
    //s2: M170c-150z -> M 170c -150z
    let s2 = s1;
    let k = 0;
    for (let i = 0; i < s1.length - 1; i++) {
        if ( isLetter( s1.charAt(i) ) && !( s1.charAt(i+1) == ' ') ) {
            s2 = s2.slice(0, i + 1 + k) + " " + s2.slice(i + 1 + k, s2.length);
            k++;
        }
    }

    //s2: M 170c -150z -> M 170 c -150 z
    s1 = s2;
    k = 0;
    for (let i = 1; i < s1.length; i++){
        if ( isLetter( s1.charAt(i) ) && isNumeric( s1.charAt(i - 1) ) ) {
            s2 = s2.slice(0, i + k) + " " + s2.slice(i + k, s2.length);
            k++;
        }
    }


    //s2: M150-170 c -170-200 z -> M 150 -170 c -170 -200
    s1 = s2;
    k = 0;
    for (let i = 1; i < s1.length; i++){
        if ( s1.charAt(i) == '-' && isNumeric( s1.charAt(i - 1) ) ) {
            s2 = s2.slice(0, i + k) + " " + s2.slice(i + k, s2.length);
            k++;
        }
    }


    //s2: l .15 -.17.17 -.19 -> l .15 -.17 .17 -.19
    s2 = "m " + s2;
    s1 = s2;
    k = 0;
    for (let i = s1.length - 1; i > 0; i--){
        if (s1.charAt(i) == '.'){
            let temp_i = i;
            let dotExist = false;
            while (s1.charAt(--temp_i) != ' '){
                if (s1.charAt(temp_i) == '.'){
                    dotExist = true;
                }
            }
            if (dotExist){
                s2 = s2.slice(0, i) + " " + s2.slice(i, s2.length);
                k++;
            }
        }
    }
    s2 = s2.slice(2, s2.length);


    //s2: l .15 -.17 .17 -.19 -> l 0.15 -0.17 0.17 -0.19
    s1 = s2;
    k = 0;
    for (let i = 2; i < s1.length; i++){
        if (    ( s1.charAt(i) == '.' && s1.charAt(i-1) == ' ' )
            ||  ( s1.charAt(i) == '.' && s1.charAt(i-1) == '-' && s1.charAt(i-2) == ' ' ) ) {
            s2 = s2.slice(0, i + k) + "0" + s2.slice(i + k, s2.length);
            k++;
        }
    }


    //loai bo truong hop ma: l 20 20 l 30 30 -> l 20 20 30 30, nhung m 20 20 m 30 30 -> m 20 20 m 30 30
    s1 = s2;
    k = 0;
    for (let i = 0; i < s1.length; i++){
        let c = s1.charAt(i);
        if ( isLetter(c) ) {
            while ( i++ < s1.length - 1 && !isLetter( s1.charAt(i) ) );
            
            if (i < s1.length) {
                if ( c == s1.charAt(i) && c != 'm' ){
                s2 = s2.slice(0, i + k - 1) + s2.slice(i + k + 1, s2.length);
                k = k - 2;
                }
            }
            i--;
        }
    }
    console.log(`std str = ${s2}\n`);

    return s2;
}

function extract(arr) {
    //drawCmd: M M M a a a a a a a a c c c c c c c m m m m m m l l l h h z
    let c = '?';
    for (let i = 0; i < arr.length; i++){
        if ( isLetter( arr[i] ) ){
            if (c == 'm' && arr[i] == 'm') {
                m2ndIndex.push(i);
            }
            drawCmd.push(arr[i]);
            c = arr[i];
        }
        else{
            drawCmd.push(c);
        }
    }
    m2ndIndex.push(MAX);



    //cmd:      M a c m m l h z
    //cmdCnt:   3 8 7 3 3 3 2 1
    let cntTemp = 1;
    let k = 0;
    for (let i = 1; i < drawCmd.length; i++){

        if (drawCmd[i] == drawCmd[i-1] && i != m2ndIndex[k]){
            cntTemp++;
        }

        else{
            cmd.push( drawCmd[i-1] );
            cmdCnt.push(cntTemp);
            cntTemp = 1;
            if (i == m2ndIndex[k] && k < m2ndIndex.length - 1){
                k++;
            }
        }
        
    }
    cmd.push( drawCmd[drawCmd.length-1] );
    cmdCnt.push(cntTemp);



    //cmd:      M a c m m l h z
    //cmdCnt:   1 4 3 1 1 1 1 1
    for (let i = 0; i < cmd.length; i++){

        if (cmd[i] == 'M' || cmd[i] == 'm'
            || cmd[i] == 'L' || cmd[i] == 'l'
            || cmd[i] == 'Q' || cmd[i] == 'q'
            || cmd[i] == 'T' || cmd[i] == 't'
            || cmd[i] == 'C' || cmd[i] == 'c'
            || cmd[i] == 'S' || cmd[i] == 's'){
            cmdCnt[i] = (cmdCnt[i] - 1) / 2;
        }

        else if (   cmd[i] == 'H' || cmd[i] == 'h'
                ||  cmd[i] == 'V' || cmd[i] == 'v') {
            cmdCnt[i] = cmdCnt[i] - 1;
        }

        else if (cmd[i] == 'A' || cmd[i] == 'a') {
            cmdCnt[i] = (cmdCnt[i] - 1) / 7 * 4;
        }

    }


    drawCmd = [];
    // cmd:        M a c m m l h z
    // cmdCnt:  1 4 3 1 1 1 1 1
    // drawCmd:  M a a a a c c c m m l h z
    for (let i = 0; i < cmdCnt.length; i++) {

        for (let j = 1; j <= cmdCnt[i]; j++) {
            drawCmd.push(cmd[i]);
        }

    }


      //tach lay diem cho vao array cmdPs
    k = 0;
    for (let i = 0; i < cmd.length; i++){
        k++;
        if (cmd[i] != 'H' && cmd[i] != 'h' && cmd[i] != 'V' && cmd[i] != 'v' && cmd[i] != 'A' && cmd[i] != 'a' && cmd[i] != 'Z' && cmd[i] != 'z'){
            for (let j = 0; j < cmdCnt[i]; j++){
                tempPnt = new Point( parseFloat(arr[k]), parseFloat(arr[k+1]) );
                cmdPs.push(tempPnt);
                k = k + 2;
            }
        }

        else if (cmd[i] == 'H' || cmd[i] == 'h' || cmd[i] == 'V' || cmd[i] == 'v'){
            for (let j = 0; j < cmdCnt[i]; j++){
                if (cmd[i] == 'H' || cmd[i] == 'h'){
                    tempPnt = new Point( parseFloat(arr[k]), 0 );
                    cmdPs.push(tempPnt);
                    k = k + 1;
                }
                else{
                    tempPnt = new Point( 0, parseFloat(arr[k]) );
                    cmdPs.push(tempPnt);
                    k = k + 1;          
                }
            }
        }

        else if (cmd[i] == 'A' || cmd[i] == 'a'){
            for (let j = 0; j < cmdCnt[i]; j++){
                if (j % 4 != 1){       
                    tempPnt = new Point( parseFloat(arr[k]), parseFloat(arr[k+1]) );
                    cmdPs.push(tempPnt);
                    k = k + 2;
                }
                else{
                    tempPnt = new Point( parseFloat(arr[k]), 0 );
                    cmdPs.push(tempPnt);
                    k = k + 1;
                }
            }     
        }

        else if (cmd[i] == 'Z' || cmd[i] == 'z'){
            for (let j = 0; j < cmdCnt[i]; j++){
            tempPnt = new Point(0, 0);
            cmdPs.push(tempPnt);
            }   
        }
    }

    // console.log(drawCmd);
    // console.log(cmd);
    // console.log(cmdCnt);
    // console.log(cmdPs);

}

function toAbsCoord() {
    let c;
    
    for (let i = 0; i < cmdPs.length; i++){
      p_temp = new Point(cmdPs[i].x, cmdPs[i].y);
      absPs.push(p_temp);
    }
    
    p_temp = new Point(absPs[0].x, absPs[0].y);
    
    for (let i = 1; i < drawCmd.length; i++){
      c = drawCmd[i];
      
      if (c == 'M' || c == 'L' || c == 'Q' || c == 'T' || c == 'C' || c == 'S' || c == 'A'){
        p_temp.set(cmdPs[i].x, cmdPs[i].y);
      }
      
      else if (c == 'H'){
        p_temp.set(cmdPs[i].x, absPs[i - 1].y);
        absPs[i].set(p_temp.x, p_temp.y);
      }
      
      else if (c == 'V'){
        p_temp.set(absPs[i - 1].x, cmdPs[i].y);
        absPs[i].set(p_temp.x, p_temp.y);
      }
      
      else if (c == 'm' || c == 'l' || c == 't' || c == 'h' || c == 'v'){
        p_temp.set(p_temp.x + cmdPs[i].x, p_temp.y + cmdPs[i].y);
        absPs[i].set(p_temp.x, p_temp.y);
      }
      
      else if (c == 'q' || c == 's'){
        absPs[i].set(p_temp.x + cmdPs[i].x, p_temp.y + cmdPs[i].y);
        p_temp.set(p_temp.x + cmdPs[i + 1].x, p_temp.y + cmdPs[i + 1].y);
        absPs[i + 1].set(p_temp.x, p_temp.y);
        
        i = i + 1;
      }
      
      else if (c == 'c'){
        absPs[i].set(p_temp.x + cmdPs[i].x, p_temp.y + cmdPs[i].y);
        absPs[i + 1].set(p_temp.x + cmdPs[i + 1].x, p_temp.y + cmdPs[i + 1].y);
        p_temp.set(p_temp.x + cmdPs[i + 2].x, p_temp.y + cmdPs[i + 2].y);
          
        absPs[i + 2].set(p_temp.x, p_temp.y);
        
        i = i + 2;
      }
      
      else if (c == 'a'){
        p_temp.set(p_temp.x + cmdPs[i + 3].x, p_temp.y + cmdPs[i + 3].y);
        absPs[i + 3].set(p_temp.x, p_temp.y);
        
        i = i + 3;
      }
      
      else if (c == 'Z' || c == 'z'){
        let find_m = i;
        while (drawCmd[find_m] != 'm' && drawCmd[find_m] != 'M'){
          find_m--;
        }
        p_temp.x = absPs[find_m].x;
        p_temp.y = absPs[find_m].y;
        
        absPs[i].set(p_temp.x, p_temp.y);
      }
    }

    roundPsArr(absPs);

    // console.log(`\nabsPs:`);
    // console.log(absPs);
}

function build_drawPsArr() {

    //drawPs.clear();
    //nDrawPs.clear();
    
    let c;
    let preC = '?';
    let p_temp;
    
    nDrawPs.push(drawPs.length - 1);
    drawPs.push(new Point(0, 0));
    
    for (let i = 0; i < drawCmd.length; i++) {

        //smlar: similar
        let smlar = true;
        c = drawCmd[i];

        if (c != preC) {
            preC = c;
            smlar = false;
        }
        
        if (c == 'M' || c == 'm') {

            p_temp = absPs[i];
            drawPs.push(p_temp);

            if (!smlar){
                nDrawPs.push(drawPs.length - 2);
            }

            else{
                if (preC == 'm'){
                    nDrawPs.push(drawPs.length - 2);
                }
            }

        }
        
        if (c == 'L' || c == 'l' || c == 'H' || c == 'h' || c == 'V' || c == 'v') {
            p_temp = absPs[i];
            drawPs.push(p_temp);
        }
        
        if (c == 'Q' || c == 'q'){
            let p00, p10, p20;
            p00 = absPs[i - 1];
            p10 = absPs[i];
            p20 = absPs[i + 1];
            
            drawQuadraticCurve(p00, p10, p20);
            i = i + 1;
        }
        
        if (c == 'T' || c == 't'){
            let p00, p10, p20;
            p00 = absPs[i - 1];
            //diem p10 la diem doi xung voi ...
            p10 = new Point(2 * absPs[i - 1].x - absPs[i - 2].x, 2 * absPs[i - 1].y - absPs[i - 2].y);
            p20 = absPs[i];
            
            drawQuadraticCurve(p00, p10, p20);
        }    
        
        if (c == 'C' || c == 'c'){
            let p00, p10, p20, p30;
            p00 = absPs[i - 1];
            p10 = absPs[i];
            p20 = absPs[i + 1];
            p30 = absPs[i + 2];
            
            drawBezierCurve(p00, p10, p20, p30);
            i = i + 2;
        }
        
        if (c == 'S' || c == 's'){
            let p00, p10, p20, p30; 
            p00 = absPs[i - 1];
            //p10 la diem doi xung voi      
            p10 = new Point(2 * absPs[i - 1].x - absPs[i - 2].x, 2 * absPs[i - 1].y - absPs[i - 2].y);
            p20 = absPs[i];
            p30 = absPs[i + 1];    
            
            drawBezierCurve(p00, p10, p20, p30);
            i = i + 1;      
        }
        
        if (c == 'A' || c == 'a'){
            let p_start, p_end;
            let rx_cmd, ry_cmd;
            p_start = absPs[i - 1];
            p_end = absPs[i + 3];
            rx_cmd = absPs[i].x;
            ry_cmd = absPs[i].y;
            
            let large_flag = absPs[i + 2].x;
            let sweep_flag = absPs[i + 2].y;
            
            drawArc(p_start, rx_cmd, ry_cmd, p_end, large_flag, sweep_flag);
            i = i + 3;
        }
        
        //Khi gap lenh Z, noi lai diem ma bat dau la lenh M, da duoc danh dau tai vi tri cuoi cung tai Array nDrawPs
        if (c == 'Z' || c == 'z'){
            //p_temp = drawPs.get(nDrawPs.get(nDrawPs.length - 1) + 1);
            p_temp = absPs[i];
            drawPs.push(p_temp);
        }
    }
    
    //let p_start = new Point(200, 800);
    //let p_end = new Point(800, 800);
    //drawArc(p_start, 100, 50, p_end);
    
    ////hien thi cac diem ma se khong duoc noi do thay doi vi tri but bang lenh m hoac M
    //for (let i = 0; i < nDrawPs.length; i++){
    //  print(nDrawPs[i] + " ");
    //  println();
    //}


    //lam tron mang drawPs
    roundPsArr(drawPs);
}

function drawBezierCurve(p00, p10, p20, p30){
    //this is where the algorithm are used
   
    for (let i = 0; i < 1.01; i+= 1 / CURVE_RESOLUTION){
        let p01 = new Point((1-i) * p00.x + i * p10.x, (1-i) * p00.y + i * p10.y);
        let p11 = new Point((1-i) * p10.x + i * p20.x, (1-i) * p10.y + i * p20.y);
        let p21 = new Point((1-i) * p20.x + i * p30.x, (1-i) * p20.y + i * p30.y);
        
        let p02 = new Point((1-i) * p01.x + i * p11.x, (1-i) * p01.y + i * p11.y);
        let p12 = new Point((1-i) * p11.x + i * p21.x, (1-i) * p11.y + i * p21.y);
        
        let p03 = new Point((1-i) * p02.x + i * p12.x, (1-i) * p02.y + i * p12.y);
        
        drawPs.push(p03);
    }
 }

function clrArr() {
    drawCmd = [];
    cmd = [];
    cmdCnt = [];

    cmdPs = [];
    absPs = [];
}

function prtArr() {
    console.log(`\nnDrawPs:`);
    console.log(nDrawPs);

    console.log(`\ndrawPs:`);
    console.log(drawPs);
}

function isLetter(c) {
    return c.toLowerCase() != c.toUpperCase();
}

function isNumeric(s) {
    return !isNaN(s - parseFloat(s));
}

function roundPsArr(arr) {
    for (let i = 0; i < arr.length; i++) {
        let x = arr[i].x;
        let y = arr[i].y;

        arr[i].set( parseFloat( x.toFixed(3) ), parseFloat( y.toFixed(3) ) )
    }
}




