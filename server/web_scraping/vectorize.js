const fs = require("fs");
const path = require("path");
let potrace = require('potrace');

const WEB_SCRAPING_CONSTANTS = require("./constants.js").WEB_SCRAPING_CONSTANTS;


const VECTORIZE_DOWNLOAD_PATH = WEB_SCRAPING_CONSTANTS.VECTORIZE_DOWNLOAD_PATH;

const VECTORIZE_SRC_PATH = WEB_SCRAPING_CONSTANTS.VECTORIZE_SRC_PATH;
const VECTORIZE_CUSTOM_DES_PATH = WEB_SCRAPING_CONSTANTS.VECTORIZE_CUSTOM_DES_PATH;


let vectorize = async ({outlinedFile}, callback) => {
    console.log(VECTORIZE_SRC_PATH(outlinedFile));

    potrace.trace( VECTORIZE_SRC_PATH(outlinedFile) , function(err, svg) {
        if (err) {
            callback(null, err);
            throw err;
        }
        fs.writeFileSync(VECTORIZE_CUSTOM_DES_PATH(outlinedFile), svg);

        callback(path.basename( VECTORIZE_CUSTOM_DES_PATH(outlinedFile) ), null);
    });
}

module.exports.vectorize = vectorize;