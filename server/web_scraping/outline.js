const fs = require("fs");
const path = require("path");

const WEB_SCRAPING_CONSTANTS = require("./constants.js").WEB_SCRAPING_CONSTANTS;

const OUTLINE_DOWNLOAD_PATH = WEB_SCRAPING_CONSTANTS.OUTLINE_DOWNLOAD_PATH;

const OUTLINE_SRC_PATH = WEB_SCRAPING_CONSTANTS.OUTLINE_SRC_PATH;
const OUTLINE_CUSTOM_DES_PATH = WEB_SCRAPING_CONSTANTS.OUTLINE_CUSTOM_DES_PATH;

const selectors = {
    chooseFileBtn:      ".btn.btn-primary.btn-block",
    thresholdInp:       "#thresholdSlider",
    blurInp:            "#blurSlider",
    busyIcon:           ".fa.fa-spinner.fa-pulse.pull-right.text-info.tracingBusy.collapse",
    canvas:             `canvas[id=pictureStencil]`,
}

const OUTLINE_URL = "https://online.rapidresizer.com/photograph-to-pattern.php";

let findCanvasSrc = async({driver, By, until, srcPath, removeBgStatus, thresholdInp, blurInp, thresholdMax, blurMax}) => {

    try {
        await driver.get(OUTLINE_URL);
        console.log(`-> Đi đến trang outline: ${OUTLINE_URL}`);
        
        await driver.wait(until.elementLocated(By.css(`input[type=file]`)), 15000);
    
        console.log(OUTLINE_SRC_PATH(srcPath, removeBgStatus));
        await driver.findElement(By.css(`input[type=file]`)).sendKeys(OUTLINE_SRC_PATH(srcPath, removeBgStatus))
        console.log(`-> Chọn file thành công, chuẩn bị upload`);
    
        let thresholdSlider = await driver.findElement(By.css(`#thresholdSlider`));
        let thresholdActions = driver.actions({bridge: true});
        await thresholdActions.dragAndDrop(thresholdSlider, {x: (parseInt (340 * thresholdInp / thresholdMax) - 170), y: 0}).perform();
    
        let blurSlider = await driver.findElement(By.css(`#blurSlider`));
        let blurActions = driver.actions({bridge: true});
        await blurActions.dragAndDrop(blurSlider, {x: (parseInt (340 * blurInp / blurMax) - 170), y: 0}).perform();
        console.log(`-> Điều chỉnh hai slider thành công`);
    
        let busyIconSelector = `.fa.fa-spinner.fa-pulse.pull-right.text-info.tracingBusy.collapse`;
        let busyIcon = await driver.findElement(By.css(busyIconSelector)).getAttribute(`style`);
    
        while (busyIcon != `display: none;`) {
            busyIcon = await driver.findElement(By.css(busyIconSelector)).getAttribute(`style`);
        }
        console.log(`-> Chuyển đổi thành công, sẵn sàng download`);
    
        let canvasElement = await driver.findElement(By.css(selectors.canvas));
        let canvas_base64 = await driver.executeScript(`return arguments[0].toDataURL('image/png').split(",")[1]`, canvasElement)

        await driver.close();

        return [canvas_base64, null];

    } catch (err) {
        console.log(`-> Có lỗi khi outline, không lấy được canvas_base64: ${err}`);
        return [null, err]
    }

}

let outline = async ( {Builder, By, until, options, serviceBuilder, date, browserHeadless, srcPath, removeBgStatus, thresholdInp, blurInp, thresholdMax, blurMax} ) => {

    if (browserHeadless) {
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--no-sandbox");
    }

    let driver = await new Builder()
    .forBrowser("chrome")
    .setChromeOptions(options)
    .setChromeService(serviceBuilder)
    .build();


    let [canvas_base64, err] = await findCanvasSrc({
        driver:         driver,
        By:             By,
        until:          until,
        srcPath:        srcPath,
        removeBgStatus: removeBgStatus,
        thresholdInp:   thresholdInp,
        blurInp:        blurInp,
        thresholdMax:   thresholdMax,
        blurMax:        blurMax,
    });

    if (!err) {
        let filename = path.resolve(OUTLINE_CUSTOM_DES_PATH(date, srcPath));
        fs.writeFileSync(filename, canvas_base64, `base64`);
        return [filename, null];
    }

    else {
        // return Promise.reject(`~~~~~ Outline không thành công, vui lòng thử lại`)
        return [null, err];
    }
}

module.exports.outline = outline;