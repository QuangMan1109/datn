// const puppeteer = require("puppeteer");
const fs = require("fs");
const path = require("path");
const downloadFile = require(`../Downloader`).downloadFile;

let browser;

const WEB_SCRAPING_CONSTANTS = require("./constants.js").WEB_SCRAPING_CONSTANTS;

const REMOVEBG_DOWNLOAD_PATH = WEB_SCRAPING_CONSTANTS.REMOVEBG_DOWNLOAD_PATH;

const REMOVE_SRC_PATH = WEB_SCRAPING_CONSTANTS.REMOVE_SRC_PATH;
const REMOVEBG_CUSTOM_DES_PATH = WEB_SCRAPING_CONSTANTS.REMOVEBG_CUSTOM_DES_PATH;

const funcs = require("./functions.js").funcs;
const { resolve } = require("path");

const removeBgUrl = "https://depositphotos.com/bgremover/upload.html";

const selectors = {
    uploadInput:        `._2LnRd`,
    downloadButton:     `._g6XNX._1RfBR._GNl8Q._3M5o-`,
    removedBgImage:     `._3WVj0`,
}

let findUrl = async ( {driver, By, until, srcPath} ) => {
    try {
        //Go to the page
        await driver.get(removeBgUrl);
        console.log(`-> Chuyển đến trang https://depositphotos.com/bgremover/upload.html và thực hiện removeBg ...`);

        //Upload img
        await driver.findElement(By.css(selectors.uploadInput)).sendKeys(path.resolve(srcPath))
        console.log(`-> Đã upload thành công: ${path.resolve(srcPath)}`);

        //Click download button
        await driver.wait(until.elementLocated(By.css(selectors.downloadButton)), 15000);
        console.log(`-> Đã chuyển đổi xong, sẵn sàng download`);

        let url = await driver.findElement(By.css(selectors.removedBgImage)).getAttribute(`src`);
        console.log(`-> Tìm được url của ảnh đã removebg size 600: ${url}`);

        await driver.close();
        
        return [url, null];
    }
    catch (err) {
        return [null, err];
    }

}

let removeBackground = async ( {Builder, By, until, options, serviceBuilder, browserHeadless, srcPath}, callback ) => {    // REMOVE BACKGROUND


    if (browserHeadless) {
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--no-sandbox");
    }

    let driver = await new Builder()
    .forBrowser(`chrome`)
    .setChromeOptions(options)
    .setChromeService(serviceBuilder)
    .build();

    let [url, err] = await findUrl({
        driver:     driver,
        By:         By,
        until:      until,
        srcPath:    srcPath,
    })

    if (!err) {
        downloadFile(url, path.resolve(REMOVEBG_CUSTOM_DES_PATH(srcPath)), (filename) => {

            callback(filename);
        })

        return [`OK`, null];
    }

    else {
        return [null, err]
    }

}

module.exports.removeBackground = removeBackground;