const fs = require("fs");
const path = require("path")

let typeInput = async ( {page, selector, value} ) => {
    await page.$eval(selector, (element, value) => {
        element.value = value;
    }, value)
}

let chooseFile = async ( {page, selector, filePath, startMsg} ) => {

    try {
        await page.waitForSelector(selector);
    } catch (error) {
        console.log(`Có lỗi khi tìm selector ${selector}`);
    }

    const [fileChooser] = await Promise.all( [
        page.waitForFileChooser(),
        page.click(selector)
    ] )

    try {
        await fileChooser.accept( [filePath] )
    } catch (error) {
        console.log(`Có lỗi với filePath ${filePath}`);
    }

    console.log(`-> Chọn file thành công`);
}

let regulateSliderInput = async ( {page, selector, maxValue, yourValue} ) => {
    let slider = await page.waitForSelector(selector);

    let handle = await slider.boundingBox();

    await page.mouse.move(handle.x, handle.y + handle.height / 2);
    await page.mouse.down();
    await page.mouse.move(handle.x + handle.width / maxValue * yourValue, handle.y + handle.height / 2 )
    await page.mouse.up();
}

let downloadImg = async ( {page, selector} ) => {
    let x = await page.waitForSelector(selector);

    let handle = await x.boundingBox();
    await page.mouse.move(handle.x + handle.width / 2, handle.y + handle.height / 2);

    await page.click(selector, {
        button: `right`,
    })

    await page.waitFor(1000)


    await page.keyboard.press(`PageDown`);
    await page.keyboard.press(`Enter`);

    console.log(3);

    // await page.screenshot({
    //     path:   `./public/right-click.png`
    // })

    // await page.mouse.move(200, 50);
    // await page.click(selector, {
    //     button: `left`,
    // })


}

let verifyDownload = async ( {downloadPath, resolveValue, rejectValue, startMsg, watchMsg} ) => {
    
    if (startMsg != undefined) {
        console.log(startMsg);
    }

    return new Promise ( (resolve, reject) => {
        let to1 = setTimeout( () => {
            if (!fs.existsSync(downloadPath)){
                reject(rejectValue)
                console.log(3);
            }
            console.log(4);
            
        }, 15000)

        console.log(downloadPath);
        fs.watchFile(downloadPath, (curr, prev) => {
            // if (watchMsg != undefined) {
            //     console.log(watchMsg);
            // }
            console.log(watchMsg || ``);
            if ( fs.existsSync(downloadPath) ) {
                console.log(`-> Có sự tồn tại của file`);
                fs.unwatchFile(downloadPath);
                clearTimeout(to1);
                resolve(resolveValue);
            }
        })
    })
}

let renameFile = async ( {oldPath, newPath} ) => {
    console.log(`-> Bắt đầu đổi tên file ...`);

    return new Promise ( (resolve, reject) => {
        if ( !fs.existsSync(oldPath) ) {
            reject(`~~~~~ Có vấn đề gì đó mà không tìm thấy đường dẫn ${oldPath} này`);
        }
        else {
            try {
                fs.renameSync(oldPath, newPath);
                resolve(`-> Đổi tên file từ ${path.basename(oldPath)} sang ${path.basename(newPath)} thành công`)
            }
            catch (err) {
                // console.log(err);
                reject(`~~~~~ Có lỗi gì đó khi đổi tên file từ ${oldPath} sang ${newPath}`)
            }
        }
    })

}

module.exports.funcs = {
    typeInput:              typeInput,
    chooseFile:             chooseFile,
    regulateSliderInput:    regulateSliderInput,
    downloadImg:            downloadImg,
    verifyDownload:         verifyDownload,
    renameFile:             renameFile,
}