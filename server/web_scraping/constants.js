const { basename } = require("path");
const path = require("path");
const PAGE_LOADING_TIMEOUT = 30000;
const SELECTOR_TIMEOUT = 7000;


/*--------------- Xử lý đường dẫn ---------------*/
let nameWithoutExt = (filePath) => {
    return path.basename ( filePath.slice(0, filePath.lastIndexOf('.') ) );
}



const DOWNLOAD_PATH = path.resolve(`./public`);
const REMOVEBG_DOWNLOAD_PATH = path.resolve(`./public/removebgs`);
const OUTLINE_DOWNLOAD_PATH = path.resolve(`./public/outlines`);
const VECTORIZE_DOWNLOAD_PATH = path.resolve('./public/vectorizes');

const REMOVEBG_DOWNLOAD_DEFAULT_FILENAME = (baseNameWithoutExt) => {
    return `${baseNameWithoutExt}-depositphotos-bgremover.png`;
}
const OUTLINE_DOWNLOAD_DEFAULT_FILENAME = `Design.png`;
const VECTORIZE_DOWNLOAD_DEFAULT_FILENAME = `vector.svg`;

let pathWillBeProcessed = path.resolve(`./public/uploads/q1-ul.jpg`);
let filenameWithoutExt = nameWithoutExt(pathWillBeProcessed)
let fileExtensionName = path.extname(pathWillBeProcessed)

const REMOVEBG_SRC_PATH = pathWillBeProcessed;
const REMOVEBG_CUSTOM_DES_PATH = (srcPath) => {
    let baseNameWithoutExt = nameWithoutExt(srcPath)
    return path.resolve(`${REMOVEBG_DOWNLOAD_PATH}/${baseNameWithoutExt}-removeBg.png`);
}

const OUTLINE_SRC_PATH = (srcPath, removeBgStatus) => {
    if (removeBgStatus == `done`) {
        return REMOVEBG_CUSTOM_DES_PATH(srcPath);
    }

    else if (removeBgStatus == `not_yet`) {
        return path.resolve(srcPath);
    }
};
const OUTLINE_CUSTOM_DES_PATH = (date, srcPath) => {
    let baseNameWithoutExt = nameWithoutExt(srcPath)
    return path.resolve(`${OUTLINE_DOWNLOAD_PATH}/${baseNameWithoutExt}-${date}-outline.png`);
}

const VECTORIZE_SRC_PATH = (outlinedFile) => {
    console.log(outlinedFile);
    return path.resolve(`./public/outlines/${outlinedFile}`);
};
const VECTORIZE_CUSTOM_DES_PATH = (outlinedFile) => {
    let baseNameWithoutExt = nameWithoutExt(outlinedFile);
    return path.resolve(`${VECTORIZE_DOWNLOAD_PATH}/${baseNameWithoutExt}-vectorize.svg`);
}

module.exports.WEB_SCRAPING_CONSTANTS = {
    PAGE_LOADING_TIMEOUT: PAGE_LOADING_TIMEOUT,
    SELECTOR_TIMEOUT: SELECTOR_TIMEOUT,
    DOWNLOAD_PATH: DOWNLOAD_PATH,

    REMOVEBG_DOWNLOAD_PATH:     REMOVEBG_DOWNLOAD_PATH,
    REMOVEBG_SRC_PATH:          REMOVEBG_SRC_PATH,
    REMOVEBG_CUSTOM_DES_PATH:   REMOVEBG_CUSTOM_DES_PATH,

    OUTLINE_DOWNLOAD_PATH:      OUTLINE_DOWNLOAD_PATH,
    OUTLINE_SRC_PATH:           OUTLINE_SRC_PATH,
    OUTLINE_CUSTOM_DES_PATH:    OUTLINE_CUSTOM_DES_PATH,

    VECTORIZE_DOWNLOAD_PATH:    VECTORIZE_DOWNLOAD_PATH,
    VECTORIZE_SRC_PATH:         VECTORIZE_SRC_PATH,
    VECTORIZE_CUSTOM_DES_PATH:  VECTORIZE_CUSTOM_DES_PATH,
}

