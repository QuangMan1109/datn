// DOC FILE SVG

const fs = require("fs");

const domParser = require("dom-parser");
const parser = new domParser();

const PATH = require("path")
const MAX = 100000;
const CURVE_RESOLUTION = 15;

const MIN_DIS = 0.5;


//xay dung Point object
class Point{
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    set(x, y) {
        this.x = x;
        this.y = y;
    }
}

let sizeCanvas = {
    width:  1000,
    height: 1000,
};

let width_vp = 1000;
let height_vp = 1000;

let vb;

let drawCmd = [];
let m2ndIndex = [];
let cmd = [];
let cmdCnt = [];

let cmdPs = [];
let absPs = [];
let txPs = [];

let drawPs = [];
let nDrawPs = [];

let smlElms = [];

let path = [];

let html;

Math.toDegrees = function(rad) {
    return rad * 180 / Math.PI;
}

Math.toRadians = function(deg) {
    return deg * Math.PI / 180;
}

function sameSide(a, b, c, x1, y1, x2, y2) {
    //diem (x1, y1) nam cung phia voi (x2, y2) bo la duong thang ax + by + c = 0


    if ((a * x1 + b * y1 + c) * (a * x2 + b * y2 + c) > 0){
      return true;
    }
    else{
      return false;
    }
}

function condition1(a, b, c, x1, y1, x2, y2, cx1, cy1) {
    if ((x1 == x2 && y1 > y2 && cx1 < x1) || (x1 == x2 && y1 < y2 && cx1 > x1)
     || (y1 == y2 && x1 > x2 && cy1 > y1) || (y1 == y2 && x1 < x2 && cy1 < y2)
     || (x1 > x2 && y1 > y2 && !sameSide(a, b, c, cx1, cy1, x1, y2)) || (x1 < x2 && y1 > y2 && sameSide(a, b, c, cx1, cy1, x1, y2))
     || (x1 > x2 && y1 < y2 && sameSide(a, b, c, cx1, cy1, x1, y2)) || (x1 < x2 && y1 < y2 && !sameSide(a, b, c, cx1, cy1, x1, y2))){
      return true;
    }
    else{
      return false;
    }
}

let resetParams = () => {

    smlElms = [];
    path = [];

    drawPs = [];
    nDrawPs = [];
}

let processViews = (views) => {
    let sizeCanvas = {
        width: 1000,
        height: 1000,
    };

    let viewPortWidth = views.viewPort.width;
    let viewPortHeight = views.viewPort.height;
    

    if (viewPortWidth != null && viewPortHeight != null) {

        if (viewPortWidth.includes(`pt`)) {
            sizeCanvas.width = parseFloat( viewPortWidth.slice(0, viewPortWidth.length - 2) );
            sizeCanvas.height = parseFloat( viewPortHeight.slice(0, viewPortHeight.length - 2) );
        }

        else {
            sizeCanvas.width = parseFloat( views.viewPort.width );
            sizeCanvas.height = parseFloat( views.viewPort.height );
        }

        return sizeCanvas;
    }

    else {

        sizeCanvas.width = parseFloat( views.viewBox[2] );
        sizeCanvas.height = parseFloat( views.viewBox[3] );
        if ( isNaN( sizeCanvas.width ) || isNaN( sizeCanvas.height ) ) {

            sizeCanvas.width = 1000;
            sizeCanvas.height = 1000;
            return sizeCanvas;
        }

        else {
            return sizeCanvas;
        }
    }
}

let applyCustomZoomIndex = (customZoomIndex) => {

    sizeCanvas.width = sizeCanvas.width * customZoomIndex;
    sizeCanvas.height = sizeCanvas.height * customZoomIndex;

    for (let i = 0; i < drawPs.length; i ++) {
        drawPs[i].x = drawPs[i].x * customZoomIndex;
        drawPs[i].y = drawPs[i].y * customZoomIndex;
    }

}

let readSvgFunc = ( {svgPath, customZoomIndex} ) => {

    resetParams();
    
    let html = fs.readFileSync(svgPath, "utf-8");

    const dom = parser.parseFromString(html);

    var svgElms = dom.getElementsByTagName("svg");

    svgElms.forEach( (item) => {
        width_vp = item.getAttribute("width");
        height_vp = item.getAttribute("height");

        vb = item.getAttribute("viewBox");
        // getVB(vb);

        getElmsFromXML(item);
        getDTxFrSmlElms();
    } )

    let views = {
        viewPort: {
            width_vp:   width_vp,
            height_vp:  height_vp,
        },

        viewBox: getVB(vb),
    }

    sizeCanvas = processViews(views);

    

    // for (let i = 0; i < smlElms.length; i ++) {
    //     console.log( `${smlElms[i].nodeName}: ${smlElms[i].getAttribute("transform")}` );
    // }

    // for (let i = 0; i < d_attribute.length; i ++) {
    //     console.log(`d = ${d_attribute[i]}, transform = ${transform_attribute[i]}`);
    // }

    // console.log(path);

    let time = 0;

    path.forEach(function(item, index) {
        // console.log(`--------------ITEM ${index}--------------`);
        // console.log(item);
        // console.log(`${count++}.a ${Date.now() - time} `);
        // time = Date.now();
        clrArr();


        //chuan hoa dAtt
        stdItem = stdStr(item.d);

        //cNumsArr: chars and numbers Array
        let cNumsArr = stdItem.split(" ");

        extract(cNumsArr);

        toAbsCoord();

        transform(item.transform.type, item.transform.para);

        build_drawPsArr();
        // console.log(`${count++}.c ${Date.now() - time} `);
        // time = Date.now();
        // if (index == path.length - 1) {
        //     filterArr();
        //     prtArr();
        // }

    } );

    filterArr();
    applyCustomZoomIndex(customZoomIndex);
    prtArr();

    let vbArr = stdStr(vb).split(" ");
    return {
        views:  sizeCanvas,
        drawArrays:   {
            drawPs:     drawPs,
            nDrawPs:    nDrawPs,
        },
    }
}

// run();


function getVP() {
    return {w: width_vp, h: height_vp}
}

function getVB() {
    let vbArr = stdStr(vb).split(" ");
    
    return vbArr;
}

function getElmsFromXML(xml) {

    //c là mảng xml con, cả #text
    let c = xml.childNodes;


    //xmlArr là mảng xml con, trừ #text
    let xmlArr = [];
    for (let i = 0; i < c.length; i = i + 1){

        if ( c[i].nodeName != "#text" ) {
            xmlArr.push(c[i]);
        }

    }

    //nếu xmlArr không có con, tức là nó là elm nhỏ nhất
    if (xmlArr.length == 0) {
        smlElms.push(xml);
    }

    //nếu còn con, tiếp tục thực hiện và add attributes cho con
    else {

        for (let i = 0; i < xmlArr.length; i++) {
            addAttsToChldn(xmlArr[i]);
            getElmsFromXML(xmlArr[i]);
        }

    }
}

function addAttsToChldn(xml) {
    //lấy att của xml (là cha)
    let txAtt = xml.getAttribute("transform");

    //c là mảng xml con, cả #text
    let c = xml.childNodes;

    //xmlArr là mảng xml con, trừ #text
    let xmlArr = [];
    for (let i = 0; i < c.length; i = i + 1){

        if ( c[i].nodeName != "#text" ) {
            xmlArr.push(c[i]);
        }

    }

    //đưa att của xml (cha) cho các con
    for (let i = 0; i < xmlArr.length; i++) {
        if (txAtt == null) txAtt = "none";

        xmlArr[i].attributes.push( {name: "transform", value: txAtt} );
    }


}

function getDTxFrSmlElms() {    //get d and transfrom attributes from smlElms array
    

    for (let i = 0; i < smlElms.length; i++) {

        let d = dAttFrAllElms( smlElms[i] );
        
        let txAtt = smlElms[i].getAttribute("transform");
        txObj = sepTxAtt(txAtt)

        path.push( {d: d, transform: txObj} );
    }
}

function sepTxAtt(s) {  //separate transform attribute
    if (s == "none" || s == null){
        return {type: "none", para: "0"};
    }

    else{
        let type = "";
        for (let k = 0; k < s.length; k++){
            if ( isLetter( s.charAt(k) ) ){
                type = type + s.substring(k, k + 1);
            }
            else if (s.charAt(k) == '(') {
                type = type + ",";
            }
        }
        type = type.substring(0, type.length - 1);
        
        let para = "";
        let para_temp = "";
        for (let k = 0; k < s.length; k++){
            let k_temp = k;
            if (s.charAt(k) == '(') {
                k_temp = k + 1;
                while (s.charAt(k) != ')'){
                    k++;
                }
                para_temp = s.substring(k_temp, k);
                para_temp = stdStr(para_temp);

                // console.log(`para_temp = ${para_temp}`)
                para = para + para_temp + ",";
            }
        }
        para = para.substring(0, para.length - 1);
        
        return {type: type, para: para};
    }
   
}

function dAttFrAllElms(xml) {    //d attribute extracted from all kind of elements
    // MÔ TẢ HÀM:
    //hàm lấy ra thuộc tính d của tất cả các element
    //Thuật toán: lấy ra các thuộc tính của 1 element, chuẩn hóa chúng (stdStr), đưa chúng về  Xét từng trường hợp.
    //xml = <ellipse cx="200" cy="300" rx="100" ry="50"/>
    //d = "m 100 300 a 100 50 0 0 0 200 0 a 100 50 0 0 0 -200 0

    let elmName = xml.nodeName;
    let d = "";
    
    if (elmName == "path"){
        d = xml.getAttribute("d");
    }
    
    else if (elmName == "circle") {
        let cx = parseFloat(stdStr(xml.getAttribute("cx")));
        let cy = parseFloat(stdStr(xml.getAttribute("cy")));
        let r = parseFloat(stdStr(xml.getAttribute("r")));
            
        d = d + "m " + (cx - r) + " " + cy + " ";
        d = d + "a " + r + " " + r + " 0 0 0 " + (r * 2) + " 0";
        d = d + "a " + r + " " + r + " 0 0 0 " + (r * -2) + " 0";
    }
    
    else if (elmName == "ellipse") {
        let cx = parseFloat(stdStr(xml.getAttribute("cx")));
        let cy = parseFloat(stdStr(xml.getAttribute("cy")));
        let rx = parseFloat(stdStr(xml.getAttribute("rx")));
        let ry = parseFloat(stdStr(xml.getAttribute("ry")));
            
        d = d + "m " + (cx - rx) + " " + cy + " ";
        d = d + "a " + rx + " " + ry + " 0 0 0 " + (rx * 2) + " 0";
        d = d + "a " + rx + " " + ry + " 0 0 0 " + (rx * -2) + " 0";
    }
    
    else if (elmName == "line") {
        let x1 = parseFloat(stdStr(xml.getAttribute("x1")));
        let y1 = parseFloat(stdStr(xml.getAttribute("y1")));
        let x2 = parseFloat(stdStr(xml.getAttribute("x2")));
        let y2 = parseFloat(stdStr(xml.getAttribute("y2")));
            
        d = d + "m " + x1 + " " + y1 + " L " + x2 + " " + y2;
    }
    
    else if (elmName == "polygon") {
        points_s = xml.getAttribute("points");
        points_s = stdStr(points_s);
    //   let xy = points_s.split(" ");
        let xy = points_s.split(" ");
      
        d = d + "m " + xy[0] + " " + xy[1] + " ";
        for (let i = 2; i < xy.length; i = i + 2){
            d = d + "L " + xy[i] + " " + xy[i + 1] + " ";
        }
        d = d + "z";
    }
    
    else if (elmName == "polyline") {
        points_s = xml.getAttribute("points");
        points_s = stdStr(points_s);
        // let xy = points_s.split(" ");
        let xy = points_s.split(" ");

        d = d + "m " + xy[0] + " " + xy[1] + " ";
        for (let i = 2; i < xy.length; i = i + 2){
            d = d + "L " + xy[i] + " " + xy[i + 1] + " ";
        }
    }
    
    else if (elmName == "rect") {
        let w, h;
        let x = 0;
        let y = 0;
        let rx = 0;
        let ry = 0;
            
        w = parseFloat(stdStr(xml.getAttribute("width")));
        h = parseFloat(stdStr(xml.getAttribute("height")));
        
        if (xml.getAttribute("x") != null){
            x = parseFloat(stdStr(xml.getAttribute("x"))); 
        }
        
        if (xml.getAttribute("y") != null){
            y = parseFloat(stdStr(xml.getAttribute("y")));    
        }
        
        if (xml.getAttribute("rx") != null){
            rx = parseFloat(stdStr(xml.getAttribute("rx")));
            ry = parseFloat(stdStr(xml.getAttribute("ry")));
        }
        
        d = d + "m " + (x + rx) + " " + y + " ";
        d = d + "h " + (w - 2 * rx) + " ";
        d = d + "a " + rx + " " + ry + " 0 0 1 " + rx + " " + ry + " ";
        d = d + "v " + (h - 2 * ry) + " ";
        d = d + "a " + rx + " " + ry + " 0 0 1 " + (-1 * rx) + " " + ry + " "; 
        d = d + "h " + (2 * rx - w) + " ";
        d = d + "a " + rx + " " + ry + " 0 0 1 " + (-1 * rx) + " " + (-1 * ry) + " ";
        d = d + "v " + (2 * ry - h) + " ";
        d = d + "a " + rx + " " + ry + " 0 0 1 " + rx + " " + (-1 * ry);
    }
    
    return d;
}

function stdStr(s0) {

    //s1 la chuoi chi giu lai cac ky tu can thiet, gom '-' '.' 0 den 9, xoa khoang trang hai dau
    let s1 = s0.replace(/[^-.0-9MHVLQTCSAZmhvlqtcsaze]/g, " ").trim();

    //s2 la chuoi se tra ve, khoi tao s2 = s1
    //s2: M170c-150z -> M 170c -150z
    let s2 = s1;
    let k = 0;
    for (let i = 0; i < s1.length - 1; i++) {
        if ( isLetter( s1.charAt(i) ) && !( s1.charAt(i+1) == ' ') ) {
            s2 = s2.slice(0, i + 1 + k) + " " + s2.slice(i + 1 + k, s2.length);
            k++;
        }
    }

    //s2: M 170c -150z -> M 170 c -150 z
    s1 = s2;
    k = 0;
    for (let i = 1; i < s1.length; i++){
        if ( isLetter( s1.charAt(i) ) && isNumeric( s1.charAt(i - 1) ) ) {
            s2 = s2.slice(0, i + k) + " " + s2.slice(i + k, s2.length);
            k++;
        }
    }


    //s2: M150-170 c -170-200 z -> M 150 -170 c -170 -200
    s1 = s2;
    k = 0;
    for (let i = 1; i < s1.length; i++){
        if ( s1.charAt(i) == '-' && isNumeric( s1.charAt(i - 1) ) ) {
            s2 = s2.slice(0, i + k) + " " + s2.slice(i + k, s2.length);
            k++;
        }
    }

    //s2: M  150  160 -> M 150 160 (reduce whitespace)
    s1 = s2;
    s2 = s1.replace(/\s+/g, ' ');

    //s2: l .15 -.17.17 -.19 -> l .15 -.17 .17 -.19
    s2 = "m " + s2;
    s1 = s2;
    k = 0;
    for (let i = s1.length - 1; i > 0; i--){
        if (s1.charAt(i) == '.'){
            let temp_i = i;
            let dotExist = false;
            while (s1.charAt(--temp_i) != ' '){
                if (s1.charAt(temp_i) == '.'){
                    dotExist = true;
                }
            }
            if (dotExist){
                s2 = s2.slice(0, i) + " " + s2.slice(i, s2.length);
                k++;
            }
        }
    }
    s2 = s2.slice(2, s2.length);


    //s2: l .15 -.17 .17 -.19 -> l 0.15 -0.17 0.17 -0.19
    s1 = s2;
    k = 0;
    for (let i = 2; i < s1.length; i++){
        if (    ( s1.charAt(i) == '.' && s1.charAt(i-1) == ' ' )
            ||  ( s1.charAt(i) == '.' && s1.charAt(i-1) == '-' && s1.charAt(i-2) == ' ' ) ) {
            s2 = s2.slice(0, i + k) + "0" + s2.slice(i + k, s2.length);
            k++;
        }
    }


    //loai bo truong hop ma: l 20 20 l 30 30 -> l 20 20 30 30, nhung m 20 20 m 30 30 -> m 20 20 m 30 30
    s1 = s2;
    k = 0;
    for (let i = 0; i < s1.length; i++){
        let c = s1.charAt(i);
        if ( isLetter(c) ) {
            while ( i++ < s1.length - 1 && !isLetter( s1.charAt(i) ) );
            
            if (i < s1.length) {
                if ( c == s1.charAt(i) && c != 'm' ){
                s2 = s2.slice(0, i + k - 1) + s2.slice(i + k + 1, s2.length);
                k = k - 2;
                }
            }
            i--;
        }
    }


    //-10e-4 -> -0.001
    s1 = s2;
    k = 0;
    for (let i = 0; i < s1.length; i++) {
        if (s1[i] == 'e') {

            k = i - 2;
            while ( isNumeric( s1[k] ) || s1[k] == '-' ) {
                k--;
            }
            let k_start = k + 1;

            let f1 = parseFloat(s1.slice(k, i - 1));

            k = i + 2;
            while ( isNumeric( s1[k] ) || s1[k] == '-' ) {
                k++;
            }
            let k_end = k - 1;

            let int1 = parseInt( s1.slice(i + 2, k) );

            let f2 = f1 * Math.pow(10, int1);
            s2 = s1.slice(0, k_start) + f2.toString() + s1.slice(k_end + 1, s1.length);
        }
    }

    // console.log(`std str = ${s2}\n`);

    return s2;
}

function extract(arr) {
    //drawCmd: M M M a a a a a a a a c c c c c c c m m m m m m l l l h h z
    let c = '?';
    for (let i = 0; i < arr.length; i++){
        if ( isLetter( arr[i] ) ){
            if (c == 'm' && arr[i] == 'm') {
                m2ndIndex.push(i);
            }
            drawCmd.push(arr[i]);
            c = arr[i];
        }
        else{
            drawCmd.push(c);
        }
    }
    m2ndIndex.push(MAX);



    //cmd:      M a c m m l h z
    //cmdCnt:   3 8 7 3 3 3 2 1
    let cntTemp = 1;
    let k = 0;
    for (let i = 1; i < drawCmd.length; i++){

        if (drawCmd[i] == drawCmd[i-1] && i != m2ndIndex[k]){
            cntTemp++;
        }

        else{
            cmd.push( drawCmd[i-1] );
            cmdCnt.push(cntTemp);
            cntTemp = 1;
            if (i == m2ndIndex[k] && k < m2ndIndex.length - 1){
                k++;
            }
        }
        
    }
    cmd.push( drawCmd[drawCmd.length-1] );
    cmdCnt.push(cntTemp);



    //cmd:      M a c m m l h z
    //cmdCnt:   1 4 3 1 1 1 1 1
    for (let i = 0; i < cmd.length; i++){

        if (cmd[i] == 'M' || cmd[i] == 'm'
            || cmd[i] == 'L' || cmd[i] == 'l'
            || cmd[i] == 'Q' || cmd[i] == 'q'
            || cmd[i] == 'T' || cmd[i] == 't'
            || cmd[i] == 'C' || cmd[i] == 'c'
            || cmd[i] == 'S' || cmd[i] == 's'){
            cmdCnt[i] = (cmdCnt[i] - 1) / 2;
        }

        else if (   cmd[i] == 'H' || cmd[i] == 'h'
                ||  cmd[i] == 'V' || cmd[i] == 'v') {
            cmdCnt[i] = cmdCnt[i] - 1;
        }

        else if (cmd[i] == 'A' || cmd[i] == 'a') {
            cmdCnt[i] = (cmdCnt[i] - 1) / 7 * 4;
        }

    }


    drawCmd = [];
    // cmd:        M a c m m l h z
    // cmdCnt:  1 4 3 1 1 1 1 1
    // drawCmd:  M a a a a c c c m m l h z
    for (let i = 0; i < cmdCnt.length; i++) {

        for (let j = 1; j <= cmdCnt[i]; j++) {
            drawCmd.push(cmd[i]);
        }

    }


    //tach lay diem cho vao array cmdPs
    k = 0;
    for (let i = 0; i < cmd.length; i++){
        k++;
        if (cmd[i] != 'H' && cmd[i] != 'h' && cmd[i] != 'V' && cmd[i] != 'v' && cmd[i] != 'A' && cmd[i] != 'a' && cmd[i] != 'Z' && cmd[i] != 'z'){
            for (let j = 0; j < cmdCnt[i]; j++){
                tempPnt = new Point( parseFloat(arr[k]), parseFloat(arr[k+1]) );
                cmdPs.push(tempPnt);
                k = k + 2;
            }
        }

        else if (cmd[i] == 'H' || cmd[i] == 'h' || cmd[i] == 'V' || cmd[i] == 'v'){
            for (let j = 0; j < cmdCnt[i]; j++){
                if (cmd[i] == 'H' || cmd[i] == 'h'){
                    tempPnt = new Point( parseFloat(arr[k]), 0 );
                    cmdPs.push(tempPnt);
                    k = k + 1;
                }
                else{
                    tempPnt = new Point( 0, parseFloat(arr[k]) );
                    cmdPs.push(tempPnt);
                    k = k + 1;          
                }
            }
        }

        else if (cmd[i] == 'A' || cmd[i] == 'a'){
            for (let j = 0; j < cmdCnt[i]; j++){
                if (j % 4 != 1){       
                    tempPnt = new Point( parseFloat(arr[k]), parseFloat(arr[k+1]) );
                    cmdPs.push(tempPnt);
                    k = k + 2;
                }
                else{
                    tempPnt = new Point( parseFloat(arr[k]), 0 );
                    cmdPs.push(tempPnt);
                    k = k + 1;
                }
            }     
        }

        else if (cmd[i] == 'Z' || cmd[i] == 'z'){
            for (let j = 0; j < cmdCnt[i]; j++){
            tempPnt = new Point(0, 0);
            cmdPs.push(tempPnt);
            }   
        }
    }

    // console.log(drawCmd);
    // console.log(cmd);
    // console.log(cmdCnt);
    // console.log(cmdPs);

    // console.log("\ncmdPs:");
    // for (let i = 0; i < cmdPs.length; i++) {

    //     console.log(`cmdPs[${i}]: ${cmdPs[i].x}, ${cmdPs[i].y}`);
    // }

}

function toAbsCoord() {
    let c;
    
    for (let i = 0; i < cmdPs.length; i++){
      p_temp = new Point(cmdPs[i].x, cmdPs[i].y);
      absPs.push(p_temp);
    }
    
    p_temp = new Point(absPs[0].x, absPs[0].y);
    
    for (let i = 1; i < drawCmd.length; i++){
      c = drawCmd[i];
      
      if (c == 'M' || c == 'L' || c == 'Q' || c == 'T' || c == 'C' || c == 'S' || c == 'A'){
        p_temp.set(cmdPs[i].x, cmdPs[i].y);
      }
      
      else if (c == 'H'){
        p_temp.set(cmdPs[i].x, absPs[i - 1].y);
        absPs[i].set(p_temp.x, p_temp.y);
      }
      
      else if (c == 'V'){
        p_temp.set(absPs[i - 1].x, cmdPs[i].y);
        absPs[i].set(p_temp.x, p_temp.y);
      }
      
      else if (c == 'm' || c == 'l' || c == 't' || c == 'h' || c == 'v'){
        p_temp.set(p_temp.x + cmdPs[i].x, p_temp.y + cmdPs[i].y);
        absPs[i].set(p_temp.x, p_temp.y);
      }
      
      else if (c == 'q' || c == 's'){
        absPs[i].set(p_temp.x + cmdPs[i].x, p_temp.y + cmdPs[i].y);
        p_temp.set(p_temp.x + cmdPs[i + 1].x, p_temp.y + cmdPs[i + 1].y);
        absPs[i + 1].set(p_temp.x, p_temp.y);
        
        i = i + 1;
      }
      
      else if (c == 'c'){
        absPs[i].set(p_temp.x + cmdPs[i].x, p_temp.y + cmdPs[i].y);
        absPs[i + 1].set(p_temp.x + cmdPs[i + 1].x, p_temp.y + cmdPs[i + 1].y);
        p_temp.set(p_temp.x + cmdPs[i + 2].x, p_temp.y + cmdPs[i + 2].y);
          
        absPs[i + 2].set(p_temp.x, p_temp.y);
        
        i = i + 2;
      }
      
      else if (c == 'a'){
        p_temp.set(p_temp.x + cmdPs[i + 3].x, p_temp.y + cmdPs[i + 3].y);
        absPs[i + 3].set(p_temp.x, p_temp.y);
        
        i = i + 3;
      }
      
      else if (c == 'Z' || c == 'z'){
        let find_m = i;
        while (drawCmd[find_m] != 'm' && drawCmd[find_m] != 'M'){
          find_m--;
        }
        p_temp.x = absPs[find_m].x;
        p_temp.y = absPs[find_m].y;
        
        absPs[i].set(p_temp.x, p_temp.y);
      }
    }

    roundPsArr(absPs, 3);

    // console.log(`\nabsPs:`);
    // console.log(absPs);

    // console.log("\nabsPs:");
    // for (let i = 0; i < absPs.length; i++) {

    //     console.log(`absPs[${i}]: ${absPs[i].x}, ${absPs[i].y}`);
    // }
}

function build_drawPsArr() {

    //drawPs.clear();
    //nDrawPs.clear();
    
    let c;
    let preC = '?';
    let p_temp;
    
    nDrawPs.push(drawPs.length - 1);
    drawPs.push(new Point(0, 0));
    
    for (let i = 0; i < drawCmd.length; i++) {

        //smlar: similar
        let smlar = true;
        c = drawCmd[i];

        if (c != preC) {
            preC = c;
            smlar = false;
        }
        
        if (c == 'M' || c == 'm') {

            p_temp = absPs[i];
            drawPs.push(p_temp);

            if (!smlar){
                nDrawPs.push(drawPs.length - 2);
            }

            else{
                if (preC == 'm'){
                    nDrawPs.push(drawPs.length - 2);
                }
            }

        }
        
        if (c == 'L' || c == 'l' || c == 'H' || c == 'h' || c == 'V' || c == 'v') {
            p_temp = absPs[i];
            drawPs.push(p_temp);
        }
        
        if (c == 'Q' || c == 'q'){
            let p00, p10, p20;
            p00 = absPs[i - 1];
            p10 = absPs[i];
            p20 = absPs[i + 1];
            
            drawQuadraticCurve(p00, p10, p20);
            i = i + 1;
        }
        
        if (c == 'T' || c == 't'){
            let p00, p10, p20;
            p00 = absPs[i - 1];
            //diem p10 la diem doi xung voi ...
            p10 = new Point(2 * absPs[i - 1].x - absPs[i - 2].x, 2 * absPs[i - 1].y - absPs[i - 2].y);
            p20 = absPs[i];
            
            drawQuadraticCurve(p00, p10, p20);
        }    
        
        if (c == 'C' || c == 'c'){
            let p00, p10, p20, p30;
            p00 = absPs[i - 1];
            p10 = absPs[i];
            p20 = absPs[i + 1];
            p30 = absPs[i + 2];
            
            drawBezierCurve(p00, p10, p20, p30);
            i = i + 2;
        }
        
        if (c == 'S' || c == 's'){
            let p00, p10, p20, p30; 
            p00 = absPs[i - 1];
            //p10 la diem doi xung voi      
            p10 = new Point(2 * absPs[i - 1].x - absPs[i - 2].x, 2 * absPs[i - 1].y - absPs[i - 2].y);
            p20 = absPs[i];
            p30 = absPs[i + 1];    
            
            drawBezierCurve(p00, p10, p20, p30);
            i = i + 1;      
        }
        
        if (c == 'A' || c == 'a'){
            let p_start, p_end;
            let rx_cmd, ry_cmd;
            p_start = absPs[i - 1];
            p_end = absPs[i + 3];
            rx_cmd = absPs[i].x;
            ry_cmd = absPs[i].y;
            
            let large_flag = absPs[i + 2].x;
            let sweep_flag = absPs[i + 2].y;
            
            drawArc(p_start, rx_cmd, ry_cmd, p_end, large_flag, sweep_flag);
            i = i + 3;
        }
        
        //Khi gap lenh Z, noi lai diem ma bat dau la lenh M, da duoc danh dau tai vi tri cuoi cung tai Array nDrawPs
        if (c == 'Z' || c == 'z'){
            //p_temp = drawPs.get(nDrawPs.get(nDrawPs.length - 1) + 1);
            p_temp = absPs[i];
            drawPs.push(p_temp);
        }
    }
    
    //let p_start = new Point(200, 800);
    //let p_end = new Point(800, 800);
    //drawArc(p_start, 100, 50, p_end);
    
    ////hien thi cac diem ma se khong duoc noi do thay doi vi tri but bang lenh m hoac M
    //for (let i = 0; i < nDrawPs.length; i++){
    //  print(nDrawPs[i] + " ");
    //  println();
    //}


    //lam tron mang drawPs
    roundPsArr(drawPs, 1);
}

function drawQuadraticCurve(p00, p10, p20) {
    
    for (let i = 0; i < 1.01; i+= 1 / CURVE_RESOLUTION) {
        if (Math.abs(i - 1) < 0.001) {
            i = 1;
        }        
        
        let p01 = new Point((1-i) * p00.x + i * p10.x, (1-i) * p00.y + i * p10.y);
        let p11 = new Point((1-i) * p10.x + i * p20.x, (1-i) * p10.y + i * p20.y);
        
        let p02 = new Point((1-i) * p01.x + i * p11.x, (1-i) * p01.y + i * p11.y);
        
        drawPs.push(p02);

        if (i == 1) {
            break;
        }
    }

}

function drawBezierCurve(p00, p10, p20, p30){
    //this is where the algorithm are used
   
    for (let i = 0; i < 1.01; i+= 1 / CURVE_RESOLUTION){
        if (Math.abs(i - 1) < 0.001) {
            i = 1;
        }
        let p01 = new Point((1-i) * p00.x + i * p10.x, (1-i) * p00.y + i * p10.y);
        let p11 = new Point((1-i) * p10.x + i * p20.x, (1-i) * p10.y + i * p20.y);
        let p21 = new Point((1-i) * p20.x + i * p30.x, (1-i) * p20.y + i * p30.y);
        
        let p02 = new Point((1-i) * p01.x + i * p11.x, (1-i) * p01.y + i * p11.y);
        let p12 = new Point((1-i) * p11.x + i * p21.x, (1-i) * p11.y + i * p21.y);
        
        let p03 = new Point((1-i) * p02.x + i * p12.x, (1-i) * p02.y + i * p12.y);
        
        drawPs.push(p03);

        if (i == 1) {
            break;
        }
    }
}

function drawArc(startP, rxCmd, ryCmd, endP, largeF, sweepF) {

    if (rxCmd != 0 && ryCmd != 0){
    
        //tinh toan cac tham so cua elip: t - he so giua rx va ry o lenh A; rx, ry - ban kinh; cx, cy - toa do tam cua elip
        let dx = endP.x - startP.x;
        let dy = endP.y - startP.y;
        let t = rxCmd / ryCmd;
        let ry = Math.sqrt((dx / 2 * dx / 2) / (t * t) + (dy / 2 * dy / 2));
        let rx = ry * t;
        // console.log(`rx = ${rx}, ry = ${ry}`);
        
        let x1, y1, x2, y2;
        let cx, cy, cx1, cy1, cx2, cy2;
        
        //neu ban kinh tinh toan nho hon ban kinh o cmd (r < r_cmd) thi ban kinh elip se la ban kinh o cmd
        //khi gan r = r_cmd thi ta phai xac dinh vi tri tam cua elip bang pt (x-cx)2/rx2 + (y-cy)2/ry2 = 1
        //xac dinh duoc hai toa do tam elip, mot vi tri cho large+sweep = 0+1 hoac 1+0 ; mot vi tri cho large+sweep = 0+0 hoac 1+1
        if (rx >= rxCmd && ry >= ryCmd){
            cx = (endP.x + startP.x) / 2;
            cy = (endP.y + startP.y) / 2;
        }
        else{
            rx = rxCmd;
            ry = ryCmd;
            cx = (endP.x + startP.x) / 2;
            cy = (endP.y + startP.y) / 2;
            
            x1 = startP.x;
            y1 = startP.y;
            x2 = endP.x;
            y2 = endP.y;
            
            //ax * x + bx = ay * y + by
            let ax = 2 * -(x1 - x2);
            let bx = x1 * x1 - x2 * x2;
            let ay = 2 * -(y2 - y1);
            let by = y2 * y2 - y1 * y1;
            // console.log(`ax = ${ax}, bx = ${bx}, ay = ${ay}, by = ${by}`);
            
            //ax * x + bx = 0
            if (y1 == y2){
                cx1 = -bx / ax;
                cx2 = -bx / ax;
                

                // console.log(`kq = ${(x1 - cx1) / rx * (x1 - cx1) / rx}`);
                cy1 = Math.sqrt( (1 - (x1 - cx1) / rx * (x1 - cx1) / rx).toFixed(4) ) * ry + cy;
                cy2 = -1 * Math.sqrt( (1 - (x1 - cx1) / rx * (x1 - cx1) / rx).toFixed(4) ) * ry + cy;

                // console.log(`cx1 = ${cx1}, cy1 = ${cy1}`);
                // console.log(`cx2 = ${cx2}, cy2 = ${cy2}`);
            }
            else{
                // y = ax + b
                let a = (ax * (ry / rx) * (ry / rx)) / ay;
                let b = (bx * (ry / rx) * (ry / rx) - by) / ay;
                let c;
                
                // a0 + a1 * x + a2 * x2 = 0;
                let a2 = 1 / (rx * rx) + (a / ry) * (a / ry);
                let a1 = -2 * (x1 / (rx * rx) + (y1 - b) * a / (ry * ry));
                let a0 = (x1 / rx) * (x1 / rx) + (y1 - b) / ry * (y1 - b) / ry - 1;
                
                let delta = a1 * a1 - 4 * a2 * a0;
                // console.log(`delta = ${delta}`);
                
                cx1 = (-a1 + Math.sqrt(delta)) / (2 * a2);
                cy1 = a * cx1 + b;
                
                cx2 = (-a1 - Math.sqrt(delta)) / (2 * a2);
                cy2 = a * cx2 + b;
                // console.log(`cx1 = ${cx1}, cy1 = ${cy1}`);
                // console.log(`cx2 = ${cx2}, cy2 = ${cy2}`);
            }
        
            //chuan hoa lai gia tri large va sweep
            // console.log(`largeF = ${largeF}, sweepF = ${sweepF}`);
            if (largeF != 0){
                largeF = 1;
            }
            if (sweepF != 0){
                sweepF = 1;
            }
            
            //phuong trinh di qua 2 diem p_start va p_end: ax + by + c = 0;    
            let a = y1 - y2;
            let b = -(x1 - x2);
            let c = -x1 * (y1 - y2) + y1 * (x1 - x2);
            
            if (largeF + sweepF != 1){//00 hoac 11
                if ( condition1(a, b, c, x1, y1, x2, y2, cx1, cy1) ){
                    cx = cx1;
                    cy = cy1;
                }
                else{
                    cx = cx2;
                    cy = cy2;
                }
            }
            else{
                if ( condition1(a, b, c, x1, y1, x2, y2, cx1, cy1) ){
                    cx = cx2;
                    cy = cy2;
                }
                else{
                    cx = cx1;
                    cy = cy1;
                }
            }  
        }
        
        // console.log("dx = " + dx + ", dy = " + dy);
        // console.log("rx = " + rx + ", ry = " + ry);
        // console.log(`center (cx, cy) = (${cx}, ${cy})`);
        
        
        //x = rx * cos(deg) + cx; y = ry * sin(deg) + cy
        let deg_start = Math.toDegrees(Math.atan2((startP.y - cy) / ry, (startP.x - cx) / rx));
        let deg_end = Math.toDegrees(Math.atan2((endP.y - cy) / ry, (endP.x - cx) / rx));

        if (deg_start < 0){
            deg_start = deg_start + 360;
        }
        if (deg_end <= 0){
            deg_end = deg_end + 360;
        }
        // console.log("goc deg_start = " + deg_start);
        // console.log("goc deg_end = " + deg_end);
        // console.log("-----------");
        
        
        //00 -> quay nguoc, 01 -> quay thuan, 10 -> quay nguoc, 11 -> quay thuan
        //sweep_flag = 0 -> quay nguoc / sweep_flag = 1 -> quay thuan
        if (sweepF == 1){//quay thuan tu p_start -> p_end bang cach cho gia tri deg_start < deg_end
        let deg;
        let deg_start_temp = deg_start;
        if (deg_start < deg_end){
        }
        else{
            deg_start = deg_start - 360;
        }
        for (let i = 0; i <= 20; i++){
            deg = deg_start + (deg_end - deg_start) / 20.0 * i;
            
            let x = rx * Math.cos(Math.toRadians(deg)) + cx;
            let y = ry * Math.sin(Math.toRadians(deg)) + cy;
            drawPs.push(new Point(x, y));
        }
        deg_start = deg_start_temp;
        }
        else{//quay nguoc tu p_start -> p_end bang cach cho gia tri deg_start > deg_end
            let deg;
            let deg_end_temp = deg_end;
            if (deg_start > deg_end){
            }
            else{
                deg_end = deg_end - 360;
            }
            for (let i = 0; i <= 20; i++){
                deg = deg_start + (deg_end - deg_start) / 20.0 * i;
                let x = rx * Math.cos(Math.toRadians(deg)) + cx;
                let y = ry * Math.sin(Math.toRadians(deg)) + cy;

                // console.log(`x = ${x}, y = ${y}`);
                drawPs.push(new Point(x, y));
            }
            deg_end = deg_end_temp;
        }
    
    }
}

function transform(cmd, para) {
  
    //nếu transform_att gồm nhiều cmd cùng lúc: translate và scale, translate và rotate
    if (cmd.includes(",")){
        let cmd_splited = cmd.split(",");
        let para_splited = para.split(",");
        
        let i_temp = 0;
        for (let i = 0; i < cmd_splited.length; i++){
            if ( cmd_splited[i] == "translate") {
                i_temp = i;
                i++;
            }
            transform( cmd_splited[i], para_splited[i] );
        }
        transform( cmd_splited[i_temp], para_splited[i_temp] );
    }
    else{
        if (cmd == "none") {
        }
        
        else if (cmd == "translate") {
            let para_array = para.split(" ");
            // console.log(para_array);
            
            let e = 0;
            let f = 0;
            
            if (para_array.length == 1){
                e = parseFloat(para_array[0]);
                console.log("hihi") 
            }
            else if (para_array.length == 2){
                e = parseFloat(para_array[0]);
                f = parseFloat(para_array[1]);
            }
            
            let matrix_para = "1 0 0 1 " + e + " " + f;
            transform("matrix", matrix_para);
        }
        
        else if (cmd == "skewX") {
            let para_array = para.split(" ");
            let deg = parseFloat(para_array[0]);
            
            let c = Math.tan(Math.toRadians(deg));
            
            let matrix_para = "1 0 " + c + " 1 0 0";
            transform("matrix", matrix_para);
        }
        
        else if (cmd == "skewY") {
            let para_array = para.split(" ");
            let deg = parseFloat(para_array[0]);
            
            let b = Math.tan(Math.toRadians(deg));
            
            let matrix_para = "1 " + b + " 0 1 0 0";
            transform("matrix", matrix_para);
        }
        
        else if (cmd == "scale") {
            let para_array = para.split(" ");
            let a = 1;
            let d = 1;
            if (para_array.length == 1){
                a = parseFloat(para_array[0]);
                d = a;
            }
            else if (para_array.length == 2){
                a = parseFloat(para_array[0]);
                d = parseFloat(para_array[1]);
            }
            
            let matrix_para = a + " 0 0 " + d + " 0 0";
            transform("matrix", matrix_para);
        }
        
        else if (cmd == "rotate") {
            let para_array = para.split(" ");
            let deg = 0;
            let a = 1;
            let b = 0;
            let c = 0;
            let d = 1;
            let e = 0;
            let f = 0;
            if (para_array.length == 1){
                deg = parseFloat(para_array[0]);
                
                a = Math.cos(Math.toRadians(deg));
                b = Math.sin(Math.toRadians(deg));
                c = -b;
                d = a;
            }
            else if (para_array.length == 3){
                deg = parseFloat(para_array[0]);
                e = parseFloat(para_array[1]);
                f = parseFloat(para_array[2]);
                
                let translate_para = (-e) + " " + (-f);
                transform("translate", translate_para);
                let rotate_para = toString(deg);
                transform("rotate", rotate_para);
                translate_para = e + " " + f;
                transform("translate", translate_para);      
            }
            
            let matrix_para = a + " " + b + " " + c + " " + d + " 0 0";
            transform("matrix", matrix_para);
        }  
        
        else if (cmd == "matrix") {
            let para_array = para.split(" ");
            let a = parseFloat(para_array[0]);
            let b = parseFloat(para_array[1]);
            let c = parseFloat(para_array[2]);
            let d = parseFloat(para_array[3]);
            let e = parseFloat(para_array[4]);
            let f = parseFloat(para_array[5]);

            // console.log(`a = ${a}, b = ${b}, c = ${c}, d = ${d}, e = ${e}, f = ${f}`);
        
            let oldX, oldY, newX, newY;
            for (let i = 0; i < absPs.length; i++){
                oldX = absPs[i].x;
                oldY = absPs[i].y;
                newX = a * oldX + c * oldY + e;
                newY = b * oldX + d * oldY + f;
                
                txPs[i] = new Point(newX, newY);

                // console.log(txPs[i]);
            }
            
            for (let i = 0; i < absPs.length; i++) {
                if (drawCmd[i] == 'a' || drawCmd[i] == 'A') {
                    oldX = absPs[i].x;
                    oldY = absPs[i].y;
                    newX = Math.abs(a * oldX);
                    newY = Math.abs(d * oldY);
                    txPs[i] = new Point(newX, newY);
                    
                    oldX = absPs[i + 1].x;
                    oldY = absPs[i + 1].y;
                    txPs[i + 1] = new Point(oldX, oldY);
                    
                    oldX = absPs[i + 2].x;
                    oldY = absPs[i + 2].y;
                    if (a * d < 0){
                        if (oldY != 0){
                        newY = 0;
                        }
                        else{
                        newY = 1;
                        }
                    }
                    else{
                        newY = oldY;
                    }
                    txPs[i + 2] = new Point(oldX, newY);
                    
                    oldX = absPs[i + 3].x;
                    oldY = absPs[i + 3].y;

                    newX = a * oldX + e;
                    newY = d * oldY + f;

                    txPs[i + 3].set(newX, newY);
                    
                    i = i + 3;
                }
            }
        }
        
        for (let i = 0; i < txPs.length; i++) {
            absPs[i] = new Point( txPs[i].x , txPs[i].y );
        }
        
        // for (let i = 0; i < absPs.length; i++){
        //     console.log(`absPs[${i}] = (${absPs[i].x}, ${absPs[i].y})`);
        // }
    }
}

function clrArr() {
    drawCmd = [];
    cmd = [];
    cmdCnt = [];

    cmdPs = [];
    absPs = [];
    txPs = [];
}

function filterArr() {  //giảm số điểm ở mảng drawPs, với khoảng cách tối thiểu là MIN_DIS
    let drawPsRep = drawPs;
    let nDrawPsRep = nDrawPs;
    // console.log(drawPsRep);
    drawPs = [];
    nDrawPs = [];

    let preX = drawPsRep[0].x;
    let preY = drawPsRep[0].y;
    let x = 0;
    let y = 0;
    let len = 0;

    let k = 0;

    for (let i = 0; i < drawPsRep.length; i++) {
        x = drawPsRep[i].x;
        y = drawPsRep[i].y;

        len = Math.sqrt( (x - preX) * (x - preX) + (y - preY) * (y - preY) );

        if (i == nDrawPsRep[k] + 1) {
            let p = new Point(drawPsRep[i].x, drawPsRep[i].y)
            drawPs.push(p);
            preX = x;
            preY = y;

            nDrawPs.push(drawPs.length - 2);
            if (k < nDrawPsRep.length - 1) {
                k++;
            }
        }

        else if (len >= MIN_DIS) {
            let p = new Point(drawPsRep[i].x, drawPsRep[i].y)
            drawPs.push(p);
            preX = x;
            preY = y;
        }
    }
}

function prtArr() {
    // for (let i = 0; i < absPs.length; i++){
    //     console.log(`absPs[${i}] = (${absPs[i].x}, ${absPs[i].y})`);
    // }

    // console.log(`\nnDrawPs:`);
    // console.log(nDrawPs);

    // console.log(drawPs);

    // console.log(`\ndrawPs:`);
    // for (let i = 0; i < drawPs.length; i++) {
    //     console.log(`drawPs[${i}] = { ${drawPs[i].x} , ${drawPs[i].y} }`);
    // }
}

function isLetter(c) {
    return c.toLowerCase() != c.toUpperCase();
}

function isNumeric(s) {
    return !isNaN(s - parseFloat(s));
}

function roundPsArr(arr, prc) {
    for (let i = 0; i < arr.length; i++) {
        let x = arr[i].x;
        let y = arr[i].y;

        arr[i].set( parseFloat( x.toFixed(prc) ), parseFloat( y.toFixed(prc) ) )
    }
}


// module.exports.Point = Point;
// module.exports.MAX = MAX;

// console.log(`w2 = ${width_vp}, h2 = ${height_vp}`);

module.exports.readSvgFunc = readSvgFunc;
module.exports.roundPsArr = roundPsArr;
module.exports.readSvgConstants = {
    Point:      Point,
    MAX:        MAX,
}
// module.exports.getVP = getVP;
// module.exports.getVB = getVB;

// module.exports.drawPs = drawPs;
// module.exports.nDrawPs = nDrawPs;