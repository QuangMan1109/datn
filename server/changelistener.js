// x = {
//     aInternal: 10,
//     aListener: (val) => {console.log(1);},

//     set a(val) {
//         this.aInternal = val;
//         this.aListener(val);
//         console.log(2);
//     },

//     get a() {
//         return this.aInternal;
//     },

//     registerListener: (listener) => {
//         console.log(4);
//         this.aListener = listener;
//         console.log(5);
//     }
// }

// x.registerListener( (val) => {
//     console.log(`someone changed x.a value to ${val}`);
// } )

// console.log(x);
// x.a = 42;
// console.log(x);

x = {
    aInternal: 10,
    aListener: (val) => {console.log(1);},
    set a(val) {
      this.aInternal = val;
      this.aListener(val);
      console.log(2);
    },
    get a() {
      return this.aInternal;
    },
    registerListener: function(listener) {
      this.aListener = listener;
      console.log(5);
    }
  }

  x.registerListener((val) => {
    console.log("Someone changed the value of x.a to " + val);
  });

  x.a = 42;