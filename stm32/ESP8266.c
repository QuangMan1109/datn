#include "ESP8266.h"

volatile char rx_string[BUFF_LEN * 3];
volatile char tx_string[100];

volatile uint8_t done_sig;

void checkConnect(void) {
	int k = 0;
	/********* AT+RST **********/
	sendString("AT+RST\r\n");
	delayUs(1000);
	
	k = idx;
	while ( !(rx_string[k - 1] == 'I' && rx_string[k] == 'P') ) {
		if (k > 0) {
			k--;
		}
		else {
			k = idx;
		}
	}
	
	sendStringToPC("Done connecting\n");
}

void initESP(void){
//	int lenGET = strlen(GET_REQ);
	
	done_sig = 0;
	
	/********* AT **********/
	sendString("AT\r\n");
	delayUs(1000);
	
	while (!waitFor("OK\r\n"));
	
	/********* AT+CWMODE=1 **********/
	sendString("AT+CWMODE_DEF=1\r\n");
	delayUs(1000);
	
	while (!waitFor("OK\r\n"));
	
	
	/********* AT+CWJAP="SSID","PASSWD" **********/
	sprintf(tx_string, "AT+CWJAP_DEF=\"%s\",\"%s\"\r\n", WIFI_SSID, WIFI_PASS);
	sendString(tx_string);
	
	while (!waitFor("OK\r\n"));
	sendStringToPC("PC:1, ");
	
//	/********* AT+CIFSR **********/
//	sendString("AT+CIFSR\r\n");

//	/********* AT+CIPSTART="TCP","http://esp8266-sendreq.herokuapp.com",80 **********/
//	
//	sprintf(tx_string, "AT+CIPSTART=\"TCP\",\"%s\",80\r\n", URL);
//	sendString(tx_string);
//	while (!waitFor("OK\r\n"));
	
}

void sendCIPSTART(void) {
	
	/********* AT+CIPSTART="TCP","http://esp8266-sendreq.herokuapp.com",80 **********/
	
	sendString("AT+CIPSTART=\"TCP\",\"%s\",80\r\n", URL);
	while (!waitFor("OK\r\n"));
	
	sendStringToPC("PC:2, ");	
	
}

void sendCIPSEND(unsigned int n) {

	/********* AT+CIPSEND=22 **********/
	sendString("AT+CIPSEND=%d\r\n", n);
	while	(!waitFor(">"));
	
	sendStringToPC("%d\n", n);
}

void sendGETHTTP(int nth) {
/********* GET /test HTTP/1.0 **********/
	
//		sprintf(tx_string, "%s\r\n\r\n", GET_REQ);
	sendString("%s%d%s\r\n\r\n", GET_1, nth, GET_2);

	while	( !waitFor("]}") );
	togglePin(GPIOC, 13);
	detectDoneSignal();
	

	extract_nDrawPs();
	extract_drawPs();
	
	sendStringToPC("nth = %d\n", nth);
	sendStringToPC("----------------\n");
//	delayMs(3000);
//	nth++;
//	len_nth = countInt(nth);	
}

void detectDoneSignal(void) {
	
//	sendStringToPC("rx = %s", rx_string);
	for (unsigned int i = 0; i < idx; i++) {
		
		//check xem xuat hien done chua
		if (rx_string[i] == 34) 
			if (rx_string[i + 1] == 'd')
				if (rx_string[i + 5] == 34) {
					done_sig = 1;
					break;
				}
				
	}
	
//	sendStringToPC("rx = %s", rx_string);
}

void extract_nDrawPs(void) {
	
	volatile unsigned int k = 0;
	volatile unsigned int i_start = 0;
	volatile unsigned int i_end = 0;
	
	char ndraw_str[BUFF_LEN];
	
	char *token;
	char token_copy[10];
	int int_token;
	
//	sendStringToPC("idx = %d\n", idx);
	
	for (volatile unsigned int i = 0; i < idx; i++) {
		if ( rx_string[i] == ']' ) {
			
			k = i;
			while ( rx_string[k] != '[' ) {
				k++;
			}
			i_start = k;
			
			while ( rx_string[k] != ']' ) {
				k++;
			}
			i_end = k;
			
			break;
		}
	}
	
//	sendStringToPC("idx = %d, s = %d, e = %d\n", idx, i_start);

	k = 0;
	for (unsigned int i = i_start + 1; i <= i_end - 1; i++) {
		ndraw_str[k] = rx_string[i];
		k++;
	}
	ndraw_str[k] = '\0';
	
	ndraw_len = 0;
	token = strtok(ndraw_str, ",");
	while( token != NULL ) {
		strcpy(token_copy, token);
		
		int_token = atoi(token_copy);
		ndraw[ndraw_len] = int_token;
		ndraw_len++;
		
		token = strtok(NULL, ",");
	}
	
	sendStringToPC("\nndraw_len = %d\n", ndraw_len);
//	for (unsigned int i = 0; i < ndraw_len; i++) {
//		sendStringToPC("ndraw[%d] = %d\n", i, ndraw[i]);
//	}
	
}

void extract_drawPs(void) {
	unsigned int k = 0;
	unsigned int i_start = 0;
	unsigned int i_end = 0;
	
	char draw_str[BUFF_LEN * 3];
	
	char *token;
	double f_token;
	uint8_t x_ext = 1;
	
	draw_len = 0;
//	sendStringToPC("rx = %s", rx_string);
	
	for (unsigned int i = 0; i < idx; i++) {		
		if ( rx_string[i] == '[' ) {
			
			k = i;
			i_start = k;
			while ( rx_string[k] != ']' ) {
				k++;
			}
			i_end = k;
			
			break;
		}
	}
	
//	sendStringToPC("s = %d, e = %d", i_start, i_end);
	k = 0;
	for (unsigned int i = i_start + 1; i <= i_end - 1; i++) {
		draw_str[k] = rx_string[i];
		k++;
	}
	
//	sendStringToPC("%s\n", draw_str);
//	delayMs(100);
	
	token = strtok(draw_str, "{},\"xy:");
	while( token != NULL ) {
		
//		sendStringToPC("t = %s\n", token);
		f_token = strtod(token, NULL);
		if (x_ext == 1) {
			draw[draw_len].x = f_token;
			x_ext = 0;
		}
		else {
			draw[draw_len].y = f_token;
			draw_len++;
			
			x_ext = 1;
		}
		
		token = strtok(NULL, "{},\"xy:");
	}

	sendStringToPC("\ndraw_len = %d\n", draw_len);
//	for (unsigned int i = 0; i < draw_len; i++) {
//		sendStringToPC("draw[%d].x = %.1f, draw[%d].y = %.1f\n", i, draw[i].x, i, draw[i].y);
//		delayMs(10);
//	}
	
}


