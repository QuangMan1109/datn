#include "main.h"

const int MAX = 10000;
const double zoom_index = 1.0/1;

volatile uint32_t sys_tick = 0;
volatile uint32_t idx;

volatile uint32_t tim4_tick = 0;

unsigned int delay_1mm = 10;
unsigned int delay_lift = 15;

struct Point draw[100];
unsigned int draw_len;
uint32_t draw_alen;

volatile int ndraw[100];
volatile unsigned int ndraw_len;

uint8_t pen_up = 1;

static double pre_x;
static double pre_y;

static int pre_x_i = 0;
static int pre_y_i = 0;

static int pre_x2 = 0;
static int pre_y2 = 0;

static int x_store = 0;
static int y_store = 0;

//mode = 0: ve truc X 1 don vi, mode = 1: ve truc Y 1 don vi
static volatile uint8_t mode;
static volatile int ix;
static volatile int iy;

int main(){
	SysTick_Config(SystemCoreClock / 1000);
	initClock();
	initGPIOs();
	
	initUSART1();
	initUSART3();
	
	initTimer2();
	
	initTimer3();
	
	//timer for delayUs function
	initTimer4();
	
	initLedStatus();
	
	sendStringToPC("Start to connect to Wifi ...\n");
	delayMs(1000);
	writePin(GPIOA, LED1_PIN, 1);
	
//	checkConnect();
	initESP();
	doneConnectingToWifi();
	
	while(1){
		startButton();
		togglePin(LED1_PORT, LED1_PIN);
		delayMs(1000);

		drawSvg(0, MAX);
//		testDraw2();
		togglePin(LED1_PORT, LED1_PIN);
	}
}

static void initClock(){
	enableClockForPort(PORTC);
	
	enableClockForTimer2();
	enableClockForTimer3();
	enableClockForTimer4();
	
	enableClockForAltFunc();
	enableClockForPort(GPIOA);
	enableClockForUSART1();
	
	enableClockForPort(GPIOB);
	enableClockForUSART3();
}

static void initGPIOs(){
	//SWJ_CFG = 010: JTAG-DP Disabled and SW-DP Enabled
	AFIO->MAPR |= (1<<25);
	
	GPIO_TYPE my_gpio;
	
	//led1
	my_gpio.port = PORTC;
	my_gpio.pin = 13;
	my_gpio.mode = OUTPUT_MODE_SPEED_10MHZ;
	my_gpio.cnf = OUTPUT_GEN_PP;
	
	initGPIO(my_gpio);
	
	//led1
	my_gpio.port = PORTA;
	my_gpio.pin = 15;
	my_gpio.mode = OUTPUT_MODE_SPEED_10MHZ;
	my_gpio.cnf = OUTPUT_GEN_PP;
	
	initGPIO(my_gpio);
	
	//led2
	my_gpio.port = LED2_PORT;
	my_gpio.pin = LED2_PIN;
	my_gpio.mode = OUTPUT_MODE_SPEED_10MHZ;
	my_gpio.cnf = OUTPUT_GEN_PP;
	
	initGPIO(my_gpio);
	
	//led4
	my_gpio.port = LED4_PORT;
	my_gpio.pin = LED4_PIN;
	my_gpio.mode = OUTPUT_MODE_SPEED_10MHZ;
	my_gpio.cnf = OUTPUT_GEN_PP;
	
	initGPIO(my_gpio);	
	
	//start button
	my_gpio.port = START_BUTTON_PORT;
	my_gpio.pin = START_BUTTON_PIN;
	my_gpio.mode = INPUT_MODE;
	my_gpio.cnf = INPUT_PU_PD;
	GPIOA->ODR |= 1;
	
	initGPIO(my_gpio);
	
	//dosth button
	my_gpio.port = DOSTH_BUTTON_PORT;
	my_gpio.pin = DOSTH_BUTTON_PIN;
	my_gpio.mode = INPUT_MODE;
	my_gpio.cnf = INPUT_PU_PD;
	GPIOA->ODR |= 1;
	
	initGPIO(my_gpio);

	//dir_x_pin
	my_gpio.port = DIR_X_PORT;
	my_gpio.pin = DIR_X_PIN;
	my_gpio.mode = OUTPUT_MODE_SPEED_10MHZ;
	my_gpio.cnf = OUTPUT_GEN_PP;
	
	initGPIO(my_gpio);
	
	//step_x_pin
	my_gpio.port = STEP_X_PORT;
	my_gpio.pin = STEP_X_PIN;
	my_gpio.mode = OUTPUT_MODE_SPEED_10MHZ;
	my_gpio.cnf = OUTPUT_GEN_PP;
	
	initGPIO(my_gpio);
	
	//dir_y_pin
	my_gpio.port = DIR_Y_PORT;
	my_gpio.pin = DIR_Y_PIN;
	my_gpio.mode = OUTPUT_MODE_SPEED_10MHZ;
	my_gpio.cnf = OUTPUT_GEN_PP;
	
	initGPIO(my_gpio);
	
	//step_y_pin
	my_gpio.port = STEP_Y_PORT;
	my_gpio.pin = STEP_Y_PIN;
	my_gpio.mode = OUTPUT_MODE_SPEED_10MHZ;
	my_gpio.cnf = OUTPUT_GEN_PP;
	
	initGPIO(my_gpio);	
	
	//servo PWM PA7
	my_gpio.port = SERVO_PORT;
	my_gpio.pin = SERVO_PIN;
	my_gpio.mode = OUTPUT_MODE_SPEED_10MHZ;
	my_gpio.cnf = OUTPUT_AF_PP;
	
	initGPIO(my_gpio);
	
	//uart1 PA9 Tx
	my_gpio.port = GPIOA;
	my_gpio.pin = 9;
	my_gpio.mode = OUTPUT_MODE_SPEED_10MHZ;
	my_gpio.cnf = OUTPUT_AF_PP;
	
	initGPIO(my_gpio);
	
	//uart1 PA10 Rx
	my_gpio.port = GPIOA;
	my_gpio.pin = 10;
	my_gpio.mode = INPUT_MODE;
	my_gpio.cnf = INPUT_FLOATING;
	
	initGPIO(my_gpio);
	
	//uart3 PB10 Tx
	my_gpio.port = GPIOB;
	my_gpio.pin = 10;
	my_gpio.mode = OUTPUT_MODE_SPEED_10MHZ;
	my_gpio.cnf = OUTPUT_AF_PP;
	
	initGPIO(my_gpio);
	
	//uart3 PB11 Rx
	my_gpio.port = GPIOB;
	my_gpio.pin = 11;
	my_gpio.mode = INPUT_MODE;
	my_gpio.cnf = INPUT_FLOATING;
	
	initGPIO(my_gpio);
}




void TIM2_IRQHandler(void){
	
	/*--------------------- THUAT TOAN MOI ----------------------*/
	
	if (TIM2->SR & TIM_SR_UIF) {
		
		switch (mode) {
			case 0:
				togglePin(STEP_X_PORT, STEP_X_PIN);
				ix++;
				break;
			
			case 1:
				togglePin(STEP_Y_PORT, STEP_Y_PIN);
				iy++;
				break;
			
			case 2:			
				for (volatile int i = 0; i < 10; i++) {
					togglePin(STEP_X_PORT, STEP_X_PIN);
					togglePin(STEP_Y_PORT, STEP_Y_PIN);
				}
				
				break;
		}
	
	}
	TIM2->SR &= ~(TIM_SR_UIF);
}

static void initLedStatus(void) {
	writePin(LED1_PORT, LED1_PIN, 0);
	writePin(LED2_PORT, LED2_PIN, 0);
	writePin(LED3_PORT, LED3_PIN, 0);
	writePin(LED4_PORT, LED4_PIN, 0);
}

static void startButton(void){
//	// Khong tu dien
//	while(readPin(GPIOA, 0)){
//		delayMs(100);
//		if (!readPin(GPIOA, 0)){
//			delayMs(100);
//			break;
//		}
//	}
	
	// Co tu dien
	while (readPin(START_BUTTON_PORT, START_BUTTON_PIN)) {
		delayMs(20);
		if (!readPin(START_BUTTON_PORT, START_BUTTON_PIN)) {
			delayMs(20);
			break;
		}
	}
	
	sendStringToPC("Done pressing start button\n");
}

static void doneConnectingToWifi(void) {
	for (int i = 0; i < 11; i++) {
		togglePin(LED4_PORT, LED4_PIN);
		delayMs(50);
	}
}

static void liftPen(uint8_t lift){
	if (lift){
		for (volatile uint16_t duty = 85; duty >= 50; duty -= 5){
			TIM3->CCR2 = duty * 10;
			delayMs(delay_lift);
		}
		
		pen_up = 1;
	}
	else{
		for (volatile uint16_t duty = 50; duty <= 85; duty += 5){
			TIM3->CCR2 = duty * 10;
			delayMs(delay_lift);
		}
		
		pen_up = 0;
	}
	
	delayMs(delay_lift);
}


/*---------------------THUAT TOAN MOI---------------------*/
void moveX1mm(void) {
	mode = 0;
	volatile int step = 2;
	ix = 0;
	
	uint16_t arr_value = 20;
	TIM2->ARR = arr_value;
	
	TIM2->CR1 |= TIM_CR1_CEN;
	
	while (ix < step);
	
	TIM2->CR1 &= ~(TIM_CR1_CEN);
	
	delayMs(delay_1mm);
}

void moveY1mm(void) {
	mode = 1;
	volatile int step = 2;
	iy = 0;
	
	uint16_t arr_value = 20;
	TIM2->ARR = arr_value;
	
	TIM2->CR1 |= TIM_CR1_CEN;
	
	while (iy < step);
	
	TIM2->CR1 &= ~(TIM_CR1_CEN);
	
	delayMs(delay_1mm);
}


void drawXY(int dx, int dy) {
	// dx = 1 -> ~0.2mm
//	dx = dx * 5;
//	dy = dy * 5;
//	sendStringToPC("dx = %d, dy = %d\n", dx, dy);
	setDir(dx, dy);
	drawAbsXY(abs(dx), abs(dy));
}

void setDir(int dx, int dy) {
	if (dx > 0) {
		writePin(DIR_X_PORT, DIR_X_PIN, 0);
	}
	else{
		writePin(DIR_X_PORT, DIR_X_PIN, 1);
	}
	
	if (dy > 0) {
		writePin(DIR_Y_PORT, DIR_Y_PIN, 0);
	}
	else{
		writePin(DIR_Y_PORT, DIR_Y_PIN, 1);		
	}
}

void drawAbsXY(int dx, int dy) {
	
	//xet dx, dy = 0?
	if (dx == 0) {
		for (int i = 0; i < dy; i++) {
			moveY1mm();
		}
		return;
	}
	
	if (dy == 0) {
		for (int i = 0; i < dx; i++) {
			moveX1mm();
		}
		return;
	}
	
	//neu dx va dy deu khac 0
	int k = ucln(dx, dy);
	
	if (k != 1) {
		for (int i = 0; i < k; i++) {
			drawAbsXY( dx/k , dy/k );
		}
	}
	
	else {
	
		int dx1, dx2, dy1, dy2;
		if (dx > dy) {
			if (dy != 1) {
				dx1 = dx / 2;
				dx2 = dx - dx1;
				dy1 = dy / 2;
				dy2 = dy - dy1;
				
				drawAbsXY(dx1, dy1);
				drawAbsXY(dx2, dy2);
				
			}
			
			else {
				for (int i = 0; i < dx; i++) {
					moveX1mm();
				}
//				delayMs(5);
				moveY1mm();
				
			}
		}
		
		else if (dx < dy) {
			if (dx != 1) {
				dx1 = dx / 2;
				dx2 = dx - dx1;
				dy1 = dy / 2;
				dy2 = dy - dy1;
				
				drawAbsXY(dx1, dy1);
				drawAbsXY(dx2, dy2);
				
			}
			
			else {
				moveX1mm();
//				delayMs(5);
				for (int i = 0; i < dy; i++) {
					moveY1mm();
				}
			}	
		
		}
		
		//dx = dy = 1
		else if (dx == dy) {		
			moveX1mm();
//			delayMs(5);
			moveY1mm();
		}
		
	}
}

void drawCircle(int r) {
	static volatile float x = 0;
	static volatile float y = 0;

	static volatile float pre_x = 0;
	static volatile float pre_y = 0;
	
	volatile int dx = 0;
	volatile int dy = 0;
	
	for (volatile float i = 0; i <= 180; i = i + 10){
		x = r * (float)cos( (double)( i * (float)(M_PI / 180) ) );
		y = r * (float)sin( (double)( i * (float)(M_PI / 180) ) );
		
		if (i > 0){
			dx = (int) (x - pre_x);
			dy = (int) (y - pre_y);
			
//			sendStringToPC("x = %.2f, prex = %.2f, y = %.2f, prey = %.2f, dx = %d, dy = %d\n", x, pre_x, y, pre_y, dx, dy);
//			delayUs(100);
			drawXY(dx, dy);
		}
		
		pre_x = x;
		pre_y = y;
	}
	
	delayMs(20);
}

static void drawSvg(int start_nth, int end_nth) {
	
	int lenGET1 = strlen(GET_1);
	int lenGET2 = strlen(GET_2);
	
	int nth = start_nth;
	int len_nth = countInt(nth);
	draw_alen = 0;
	
	pre_x = 0;
	pre_y = 0;
	
	int restart_nth = start_nth;
	
	liftPen(1);
	
	sendCIPSTART();
	
	do {
		sendCIPSEND(lenGET1 + len_nth + lenGET2 + 4);
		sendGETHTTP(nth);
		
		draw_alen = nth * 40;
		drawNhe2();
		
		nth++;
		len_nth = countInt(nth);
		
		if (nth > restart_nth + 90) {
			checkConnect();
			sendCIPSTART();
			restart_nth = nth;
			delayMs(50);
			
		}
		
	}
	while ( (end_nth == MAX)? (!done_sig) : (nth <= end_nth) );
//	while (!done_sig);
}

static void notifyServerDoneDrawing(void) {
	char* GET3 = "GET /test?client_done=1";
	char* GET4 = " HTTP/1.1\r\nHost: esp8266-sendreq.herokuapp.com";
	unsigned int lenGET3 = strlen(GET3);
	unsigned int lenGET4 = strlen(GET4);
	
//	sendCIPSTART();
	
	sendCIPSEND(lenGET3 + lenGET4 + 4);
	
	sendString("%s%s\r\n\r\n", GET3, GET4);
	waitFor("ack");
}

static void drawNhe(void) {
	int dx, dy;
	unsigned int k = 0;
	
	
	for (unsigned int i = 0; i < draw_len; i++) {
		
		if ( i == 0 ) {
			dx = (int) ( ( draw[i].x - pre_x ) );
			dy = (int) ( ( draw[i].y - pre_y ) );
		}
		
		else {
			dx = (int) ( ( draw[i].x - draw[i - 1].x ) );
			dy = (int) ( ( draw[i].y - draw[i - 1].y ) );
		}
		
		if ( (int)draw_alen == ( ndraw[k] + 1 ) ) {
			//khong ve
			
			if (!pen_up) {
				liftPen(1);
			}
			
			if (k < ndraw_len - 1) {
				k++;
			}
		}
		
		else {
			
			if (pen_up) {
				liftPen(0);
			}
			
		}
		
		drawXY(dx, dy);
		delayMs(5);
	
		if ( i == draw_len - 1 ) {
			pre_x = draw[i].x;
			pre_y = draw[i].y;
		}
		
		draw_alen++;
		
	}
}

static void drawNhe2(void) {
	volatile int dx, dy;
	unsigned int k = 0;
	volatile int x = 0;
	volatile int y = 0;
	

	
	for (unsigned int i = 0; i < draw_len; i++) {
		
		if (draw_alen > 0) {
		
			x = (int)( (round)( draw[i].x * zoom_index ) );
			y = (int)( (round)( draw[i].y * zoom_index ) );
			
		}
		
		if (i > 0) {
			
			pre_x_i = (int)( (round)( draw[i - 1].x * zoom_index ) );
			pre_y_i = (int)( (round)( draw[i - 1].y * zoom_index ) );
		}
		
		else {
			
			pre_x_i = x_store;
			pre_y_i = y_store;
		}
		
		if ( (int)draw_alen == (ndraw[k] + 1) ) {
			//khong ve
			if (!pen_up) {
				liftPen(1);
			}
			
			if (k < ndraw_len - 1) {
				k++;
			}
			
		}
		
		else {
						
			//	Neu truoc do khong ve thi di chuyen den diem i-1 bat dau ve, roi ha but de ve den diem i
			dx = pre_x_i - pre_x2;
			dy = pre_y_i - pre_y2;
			if (pen_up) {
//				sendStringToPC("i = %d: (%d, %d), (%d, %d), (%d, %d)\n", i, dx, dy, pre_x_i, pre_y_i, pre_x2, pre_y2);
				delayMs(5);
				
				drawXY(dx, dy);
				pre_x2 = pre_x_i;
				pre_y2 = pre_y_i;
				
				liftPen(0);
				

			}
			
			// Ve den diem i
			dx = x - pre_x2;
			dy = y - pre_y2;
			
			drawXY(dx, dy);
			pre_x2 = x;
			pre_y2 = y;
			
//			sendStringToPC("i = %d: %d, %d\n", i, dx, dy);
			delayMs(5);
		}
		
		
//		dx = x - pre_x2;
//		dy = y - pre_y2;
//		if ( abs(dx) > 0 || abs(dy) > 0 ) {
//			pre_x2 = x;
//			pre_y2 = y;
//			
//			drawXY(dx, dy);
//			
////			sendStringToPC("dx = %d, dy = %d\n", dx, dy);
//		}
		
		
		draw_alen++;
		
		if (i == draw_len - 1) {
			x_store = (int)( (round)( draw[i].x * zoom_index ) );
			y_store = (int)( (round)( draw[i].y * zoom_index ) );
		}
	}
}

static void testDraw2(void) {
	
	liftPen(0);
	for (int i = 0; i < 6; i++) {
		drawXY(607, 103);
	}
//	drawXY(50, -15);
//	drawXY(-150, -25);
}





