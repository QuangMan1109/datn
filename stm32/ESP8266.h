#ifndef	_ESP8266
#define _ESP8266

#include "stm32f10x.h"                  // Device header
#include "main.h"

#define WIFI_SSID	"VIETTEL_2.4G_NP3DgK"
#define	WIFI_PASS	"thichanga"

#define URL				"http://esp8266-sendreq.herokuapp.com"
#define NTH				1
#define GET_REQ		"GET /test HTTP/1.1\r\nHost: esp8266-sendreq.herokuapp.com"
#define	GET_1			"GET /test?nth="
#define GET_2			" HTTP/1.1\r\nHost: esp8266-sendreq.herokuapp.com"

void checkConnect(void);
void initESP(void);

void sendCIPSTART(void);
void sendCIPSEND(unsigned int n);
void sendGETHTTP(int nth);

void detectDoneSignal(void);
void extract_nDrawPs(void);
void extract_drawPs(void);

#endif
