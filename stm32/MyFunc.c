
#include "MyFunc.h"

void SysTick_Handler(void){
	sys_tick++;
}

void TIM4_IRQHandler(void){
	
	if (TIM4->SR & TIM_SR_UIF){
		tim4_tick++;
	}
	
	TIM4->SR &= ~(TIM_SR_UIF);
	
}

void delayUs(uint32_t us){
	tim4_tick = 0;
	
	TIM4->CR1 |= TIM_CR1_CEN;
	
	while (tim4_tick < us);
	
	TIM4->CR1 &= ~TIM_CR1_CEN;
}

void delayMs(uint32_t ms){
	volatile uint32_t time = 0;
	time = sys_tick;		
	while ((sys_tick - time) < ms);
}

int ucln(int a, int b) {
	while (a != b) {
		if (a > b) {
			a -= b;
		}
		else {
			b -= a;
		}
	}
	
	return a;
}

int countInt(int n) {
	int temp = n;
	int cnt = 0;
	
	if(n == 0) cnt = 1;
  while(temp != 0)
  {
    cnt++;
    temp = temp / 10;
  }
	
	return cnt;
}