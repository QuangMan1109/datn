#ifndef	_MYFUNC
#define _MYFUNC

#include "stm32f10x.h"                  // Device header


/*------------------ UNIVERSAL VARIABLES ------------------*/
#define BUFF_LEN 1000

struct Point {
	double x;
	double y;
};

extern const int MAX;
extern const double zoom_index;

extern volatile uint32_t sys_tick;
extern volatile uint32_t tim4_tick;

extern volatile char rx_string[BUFF_LEN * 3];
extern volatile uint32_t idx;	//index (length) of rx_string
extern volatile char tx_string[100];

extern unsigned int delay_1mm;
extern unsigned int delay_lift;

extern struct Point draw[100];
extern unsigned int draw_len;
extern uint32_t draw_alen;

extern volatile int ndraw[100];
extern volatile unsigned int ndraw_len;

extern volatile uint8_t done_sig;





/*------------------ UNIVERSAL FUNCTIONS ------------------*/
void SysTick_Handler(void);
void TIM4_IRQHandler(void);
void delayUs(uint32_t us);
void delayMs(uint32_t ms);

int ucln(int a, int b);

int countInt(int n);

#endif
