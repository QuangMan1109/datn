#ifndef _MAIN_H
#define _MAIN_H

#include "stm32f10x.h"                  // Device header

#include <math.h>
#include "_HAL_GPIO.h"
#include "RCC.h"
#include "UART.h"
#include "Timer.h"
#include "ESP8266.h"
#include "MyFunc.h"

//define cac hang so
#define DIS_PER_REV	(double)41.52
#define ANGLE_PER_STEP (double)1.8
#define DURATION_PER_STEP 2	//1ms/1step

#define M_PI acos(-1.0)
#define DND (double)0.1	//distance not draw
	

//define ten cac ports va pins
#define DIR_X_PIN		3
#define DIR_X_PORT	GPIOA
#define	STEP_X_PIN	4
#define STEP_X_PORT	GPIOA

#define DIR_Y_PIN		5
#define	DIR_Y_PORT	GPIOA
#define STEP_Y_PIN	6
#define	STEP_Y_PORT	GPIOA

#define	SERVO_PIN		7
#define SERVO_PORT	GPIOA

#define START_BUTTON_PIN		1
#define START_BUTTON_PORT		GPIOA

#define DOSTH_BUTTON_PIN		2
#define DOSTH_BUTTON_PORT		GPIOA

#define LED1_PIN		15
#define LED1_PORT		GPIOA

#define LED2_PIN		3
#define LED2_PORT		GPIOB

#define LED3_PIN		4
#define LED3_PORT		GPIOB

#define LED4_PIN		5
#define LED4_PORT		GPIOB

//dinh nghia prototypes
static void initClock(void);
static void initGPIOs(void);
void TIM2_IRQHandler(void);

static void initLedStatus(void);
static void startButton(void);
static void doneConnectingToWifi(void);



static void liftPen(uint8_t pen_up);

void moveX1mm(void);
void moveY1mm(void);
void drawXY(int dx, int dy);
void setDir(int dx, int dy);
void drawAbsXY(int dx, int dy);

void USART1_IRQHandler(void);
void drawCircle(int r);

static void drawSvg(int start_nth, int end_nth);
static void notifyServerDoneDrawing(void);
static void testDraw2(void);

static void drawNhe(void);
static void drawNhe2(void);

#endif
